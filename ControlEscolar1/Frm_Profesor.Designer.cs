﻿namespace ControlEscolar
{
    partial class Frm_Profesor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Profesor));
            this.lbl_id = new System.Windows.Forms.Label();
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.lbl_nombre = new System.Windows.Forms.Label();
            this.dtg_profesor = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_estado = new System.Windows.Forms.ComboBox();
            this.cmd_ciudad = new System.Windows.Forms.ComboBox();
            this.txt_apellidom = new System.Windows.Forms.TextBox();
            this.txt_apellidop = new System.Windows.Forms.TextBox();
            this.txt_direccion = new System.Windows.Forms.TextBox();
            this.txt_cedula = new System.Windows.Forms.TextBox();
            this.txt_titulo = new System.Windows.Forms.TextBox();
            this.Btn_Nuevo = new System.Windows.Forms.Button();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.Btn_cancelar = new System.Windows.Forms.Button();
            this.Btn_eliminar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.grp_estudios = new System.Windows.Forms.GroupBox();
            this.btn_eliminardoctorado = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btn_eliminartitulo = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_doctpdf = new System.Windows.Forms.TextBox();
            this.txt_maestriapdf = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_estudio = new System.Windows.Forms.Label();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.BtnEliminarMaestria = new System.Windows.Forms.Button();
            this.BtnCargarMaestria = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_titupdf = new System.Windows.Forms.TextBox();
            this.dtg_estudios = new System.Windows.Forms.DataGridView();
            this.txt_fechanac = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_ida = new System.Windows.Forms.Label();
            this.txtbuscar2 = new System.Windows.Forms.TextBox();
            this.dtg_asignacion = new System.Windows.Forms.DataGridView();
            this.btn_eliminarA = new System.Windows.Forms.Button();
            this.btn_cancelarA = new System.Windows.Forms.Button();
            this.btn_guardarA = new System.Windows.Forms.Button();
            this.btn_nuevoA = new System.Windows.Forms.Button();
            this.txt_matricula = new System.Windows.Forms.TextBox();
            this.cmb_materia = new System.Windows.Forms.ComboBox();
            this.cmb_grupo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_profesor)).BeginInit();
            this.grp_estudios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_estudios)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_asignacion)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Location = new System.Drawing.Point(688, 92);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(16, 16);
            this.lbl_id.TabIndex = 0;
            this.lbl_id.Text = "0";
            this.lbl_id.Click += new System.EventHandler(this.label1_Click);
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(18, 16);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(516, 22);
            this.txt_buscar.TabIndex = 1;
            this.txt_buscar.TextChanged += new System.EventHandler(this.txt_buscar_TextChanged);
            // 
            // lbl_nombre
            // 
            this.lbl_nombre.AutoSize = true;
            this.lbl_nombre.BackColor = System.Drawing.Color.Transparent;
            this.lbl_nombre.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nombre.ForeColor = System.Drawing.Color.SkyBlue;
            this.lbl_nombre.Location = new System.Drawing.Point(15, 73);
            this.lbl_nombre.Name = "lbl_nombre";
            this.lbl_nombre.Size = new System.Drawing.Size(71, 16);
            this.lbl_nombre.TabIndex = 5;
            this.lbl_nombre.Text = "NOMBRE";
            // 
            // dtg_profesor
            // 
            this.dtg_profesor.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtg_profesor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_profesor.Location = new System.Drawing.Point(3, 371);
            this.dtg_profesor.Name = "dtg_profesor";
            this.dtg_profesor.Size = new System.Drawing.Size(1291, 200);
            this.dtg_profesor.TabIndex = 6;
            this.dtg_profesor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_profesor_CellContentClick);
            this.dtg_profesor.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_profesor_CellDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(15, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "APELLIDO MATERNO";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(152, 63);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(418, 22);
            this.txt_nombre.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(15, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "APELLIDO PATERNO";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(15, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "DIRECCION";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SkyBlue;
            this.label5.Location = new System.Drawing.Point(15, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "ESTADO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SkyBlue;
            this.label6.Location = new System.Drawing.Point(303, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "CIUDAD";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(15, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "TITULO";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.SkyBlue;
            this.label8.Location = new System.Drawing.Point(15, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 16);
            this.label8.TabIndex = 14;
            this.label8.Text = "NO_CEDULA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.SkyBlue;
            this.label9.Location = new System.Drawing.Point(15, 304);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(177, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "FECHA DE NACIMIENTO";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // cmb_estado
            // 
            this.cmb_estado.FormattingEnabled = true;
            this.cmb_estado.Location = new System.Drawing.Point(132, 178);
            this.cmb_estado.Name = "cmb_estado";
            this.cmb_estado.Size = new System.Drawing.Size(165, 24);
            this.cmb_estado.TabIndex = 16;
            this.cmb_estado.SelectedIndexChanged += new System.EventHandler(this.cmb_estado_SelectedIndexChanged);
            this.cmb_estado.Click += new System.EventHandler(this.cmb_estado_Click);
            // 
            // cmd_ciudad
            // 
            this.cmd_ciudad.FormattingEnabled = true;
            this.cmd_ciudad.Location = new System.Drawing.Point(393, 178);
            this.cmd_ciudad.Name = "cmd_ciudad";
            this.cmd_ciudad.Size = new System.Drawing.Size(177, 24);
            this.cmd_ciudad.TabIndex = 17;
            // 
            // txt_apellidom
            // 
            this.txt_apellidom.Location = new System.Drawing.Point(189, 92);
            this.txt_apellidom.Name = "txt_apellidom";
            this.txt_apellidom.Size = new System.Drawing.Size(381, 22);
            this.txt_apellidom.TabIndex = 18;
            // 
            // txt_apellidop
            // 
            this.txt_apellidop.Location = new System.Drawing.Point(189, 123);
            this.txt_apellidop.Name = "txt_apellidop";
            this.txt_apellidop.Size = new System.Drawing.Size(381, 22);
            this.txt_apellidop.TabIndex = 19;
            // 
            // txt_direccion
            // 
            this.txt_direccion.Location = new System.Drawing.Point(132, 152);
            this.txt_direccion.Name = "txt_direccion";
            this.txt_direccion.Size = new System.Drawing.Size(438, 22);
            this.txt_direccion.TabIndex = 20;
            // 
            // txt_cedula
            // 
            this.txt_cedula.Location = new System.Drawing.Point(132, 208);
            this.txt_cedula.Name = "txt_cedula";
            this.txt_cedula.Size = new System.Drawing.Size(438, 22);
            this.txt_cedula.TabIndex = 21;
            // 
            // txt_titulo
            // 
            this.txt_titulo.Location = new System.Drawing.Point(132, 239);
            this.txt_titulo.Name = "txt_titulo";
            this.txt_titulo.Size = new System.Drawing.Size(438, 22);
            this.txt_titulo.TabIndex = 22;
            // 
            // Btn_Nuevo
            // 
            this.Btn_Nuevo.Location = new System.Drawing.Point(641, 85);
            this.Btn_Nuevo.Name = "Btn_Nuevo";
            this.Btn_Nuevo.Size = new System.Drawing.Size(75, 23);
            this.Btn_Nuevo.TabIndex = 23;
            this.Btn_Nuevo.Text = "NUEVO";
            this.Btn_Nuevo.UseVisualStyleBackColor = true;
            this.Btn_Nuevo.Click += new System.EventHandler(this.Btn_Nuevo_Click);
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Location = new System.Drawing.Point(641, 114);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(75, 23);
            this.Btn_Guardar.TabIndex = 24;
            this.Btn_Guardar.Text = "GUARDAR";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // Btn_cancelar
            // 
            this.Btn_cancelar.Location = new System.Drawing.Point(641, 145);
            this.Btn_cancelar.Name = "Btn_cancelar";
            this.Btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.Btn_cancelar.TabIndex = 25;
            this.Btn_cancelar.Text = "CANCELAR";
            this.Btn_cancelar.UseVisualStyleBackColor = true;
            this.Btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // Btn_eliminar
            // 
            this.Btn_eliminar.Location = new System.Drawing.Point(641, 172);
            this.Btn_eliminar.Name = "Btn_eliminar";
            this.Btn_eliminar.Size = new System.Drawing.Size(75, 23);
            this.Btn_eliminar.TabIndex = 26;
            this.Btn_eliminar.Text = "ELIMINAR";
            this.Btn_eliminar.UseVisualStyleBackColor = true;
            this.Btn_eliminar.Click += new System.EventHandler(this.Btn_eliminar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(641, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 42);
            this.button1.TabIndex = 30;
            this.button1.Text = "AGREGAR ESTUDIOS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grp_estudios
            // 
            this.grp_estudios.Controls.Add(this.btn_eliminardoctorado);
            this.grp_estudios.Controls.Add(this.button4);
            this.grp_estudios.Controls.Add(this.btn_eliminartitulo);
            this.grp_estudios.Controls.Add(this.button2);
            this.grp_estudios.Controls.Add(this.txt_doctpdf);
            this.grp_estudios.Controls.Add(this.txt_maestriapdf);
            this.grp_estudios.Controls.Add(this.label11);
            this.grp_estudios.Controls.Add(this.label1);
            this.grp_estudios.Controls.Add(this.lbl_estudio);
            this.grp_estudios.Controls.Add(this.BtnGuardar);
            this.grp_estudios.Controls.Add(this.BtnEliminarMaestria);
            this.grp_estudios.Controls.Add(this.BtnCargarMaestria);
            this.grp_estudios.Controls.Add(this.label10);
            this.grp_estudios.Controls.Add(this.txt_titupdf);
            this.grp_estudios.Controls.Add(this.dtg_estudios);
            this.grp_estudios.Location = new System.Drawing.Point(742, 26);
            this.grp_estudios.Name = "grp_estudios";
            this.grp_estudios.Size = new System.Drawing.Size(552, 339);
            this.grp_estudios.TabIndex = 32;
            this.grp_estudios.TabStop = false;
            this.grp_estudios.Text = "ESTUDIOS";
            this.grp_estudios.Visible = false;
            this.grp_estudios.Enter += new System.EventHandler(this.grp_estudios_Enter);
            // 
            // btn_eliminardoctorado
            // 
            this.btn_eliminardoctorado.ForeColor = System.Drawing.Color.Black;
            this.btn_eliminardoctorado.Location = new System.Drawing.Point(424, 262);
            this.btn_eliminardoctorado.Name = "btn_eliminardoctorado";
            this.btn_eliminardoctorado.Size = new System.Drawing.Size(28, 23);
            this.btn_eliminardoctorado.TabIndex = 26;
            this.btn_eliminardoctorado.Text = "+";
            this.btn_eliminardoctorado.UseVisualStyleBackColor = true;
            this.btn_eliminardoctorado.Click += new System.EventHandler(this.btn_eliminardoctorado_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(382, 262);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 23);
            this.button4.TabIndex = 25;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_eliminartitulo
            // 
            this.btn_eliminartitulo.ForeColor = System.Drawing.Color.Black;
            this.btn_eliminartitulo.Location = new System.Drawing.Point(421, 208);
            this.btn_eliminartitulo.Name = "btn_eliminartitulo";
            this.btn_eliminartitulo.Size = new System.Drawing.Size(31, 23);
            this.btn_eliminartitulo.TabIndex = 24;
            this.btn_eliminartitulo.Text = "+";
            this.btn_eliminartitulo.UseVisualStyleBackColor = true;
            this.btn_eliminartitulo.Click += new System.EventHandler(this.btn_eliminartitulo_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(382, 206);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // txt_doctpdf
            // 
            this.txt_doctpdf.Location = new System.Drawing.Point(184, 266);
            this.txt_doctpdf.Name = "txt_doctpdf";
            this.txt_doctpdf.Size = new System.Drawing.Size(191, 22);
            this.txt_doctpdf.TabIndex = 22;
            // 
            // txt_maestriapdf
            // 
            this.txt_maestriapdf.Location = new System.Drawing.Point(183, 238);
            this.txt_maestriapdf.Name = "txt_maestriapdf";
            this.txt_maestriapdf.Size = new System.Drawing.Size(192, 22);
            this.txt_maestriapdf.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SkyBlue;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(101, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 16);
            this.label11.TabIndex = 20;
            this.label11.Text = "DOCTORADO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(101, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "TITULO";
            // 
            // lbl_estudio
            // 
            this.lbl_estudio.AutoSize = true;
            this.lbl_estudio.Location = new System.Drawing.Point(418, 20);
            this.lbl_estudio.Name = "lbl_estudio";
            this.lbl_estudio.Size = new System.Drawing.Size(16, 16);
            this.lbl_estudio.TabIndex = 18;
            this.lbl_estudio.Text = "0";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnGuardar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BtnGuardar.Location = new System.Drawing.Point(471, 273);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(45, 40);
            this.BtnGuardar.TabIndex = 17;
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnEliminarMaestria
            // 
            this.BtnEliminarMaestria.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BtnEliminarMaestria.Font = new System.Drawing.Font("Bodoni MT", 12F, System.Drawing.FontStyle.Bold);
            this.BtnEliminarMaestria.ForeColor = System.Drawing.Color.Black;
            this.BtnEliminarMaestria.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BtnEliminarMaestria.Location = new System.Drawing.Point(421, 233);
            this.BtnEliminarMaestria.Name = "BtnEliminarMaestria";
            this.BtnEliminarMaestria.Size = new System.Drawing.Size(32, 23);
            this.BtnEliminarMaestria.TabIndex = 14;
            this.BtnEliminarMaestria.Text = "+";
            this.BtnEliminarMaestria.UseVisualStyleBackColor = false;
            this.BtnEliminarMaestria.Click += new System.EventHandler(this.BtnEliminarMaestria_Click);
            // 
            // BtnCargarMaestria
            // 
            this.BtnCargarMaestria.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BtnCargarMaestria.Font = new System.Drawing.Font("Bodoni MT", 12F, System.Drawing.FontStyle.Bold);
            this.BtnCargarMaestria.ForeColor = System.Drawing.Color.Black;
            this.BtnCargarMaestria.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BtnCargarMaestria.Location = new System.Drawing.Point(381, 237);
            this.BtnCargarMaestria.Name = "BtnCargarMaestria";
            this.BtnCargarMaestria.Size = new System.Drawing.Size(30, 23);
            this.BtnCargarMaestria.TabIndex = 13;
            this.BtnCargarMaestria.Text = "...";
            this.BtnCargarMaestria.UseVisualStyleBackColor = false;
            this.BtnCargarMaestria.Click += new System.EventHandler(this.BtnCargarMaestria_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SkyBlue;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(101, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "MAESTRIA";
            // 
            // txt_titupdf
            // 
            this.txt_titupdf.Location = new System.Drawing.Point(183, 208);
            this.txt_titupdf.Name = "txt_titupdf";
            this.txt_titupdf.Size = new System.Drawing.Size(192, 22);
            this.txt_titupdf.TabIndex = 9;
            // 
            // dtg_estudios
            // 
            this.dtg_estudios.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtg_estudios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_estudios.Location = new System.Drawing.Point(6, 43);
            this.dtg_estudios.Name = "dtg_estudios";
            this.dtg_estudios.Size = new System.Drawing.Size(530, 150);
            this.dtg_estudios.TabIndex = 0;
            this.dtg_estudios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_estudios_CellContentClick);
            // 
            // txt_fechanac
            // 
            this.txt_fechanac.Location = new System.Drawing.Point(227, 299);
            this.txt_fechanac.Name = "txt_fechanac";
            this.txt_fechanac.Size = new System.Drawing.Size(200, 22);
            this.txt_fechanac.TabIndex = 33;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1337, 654);
            this.tabControl1.TabIndex = 34;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage1.BackgroundImage")));
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.txt_buscar);
            this.tabPage1.Controls.Add(this.dtg_profesor);
            this.tabPage1.Controls.Add(this.grp_estudios);
            this.tabPage1.Controls.Add(this.txt_fechanac);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.lbl_nombre);
            this.tabPage1.Controls.Add(this.Btn_eliminar);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.Btn_cancelar);
            this.tabPage1.Controls.Add(this.txt_nombre);
            this.tabPage1.Controls.Add(this.Btn_Guardar);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.Btn_Nuevo);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.lbl_id);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txt_titulo);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txt_cedula);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txt_direccion);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txt_apellidop);
            this.tabPage1.Controls.Add(this.cmb_estado);
            this.tabPage1.Controls.Add(this.txt_apellidom);
            this.tabPage1.Controls.Add(this.cmd_ciudad);
            this.tabPage1.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.ForeColor = System.Drawing.Color.SkyBlue;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1329, 628);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Profesores";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(540, 15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 23);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage2.BackgroundImage")));
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.lbl_ida);
            this.tabPage2.Controls.Add(this.txtbuscar2);
            this.tabPage2.Controls.Add(this.dtg_asignacion);
            this.tabPage2.Controls.Add(this.btn_eliminarA);
            this.tabPage2.Controls.Add(this.btn_cancelarA);
            this.tabPage2.Controls.Add(this.btn_guardarA);
            this.tabPage2.Controls.Add(this.btn_nuevoA);
            this.tabPage2.Controls.Add(this.txt_matricula);
            this.tabPage2.Controls.Add(this.cmb_materia);
            this.tabPage2.Controls.Add(this.cmb_grupo);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1329, 628);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Asignaciones";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(460, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // lbl_ida
            // 
            this.lbl_ida.AutoSize = true;
            this.lbl_ida.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ida.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbl_ida.Location = new System.Drawing.Point(460, 42);
            this.lbl_ida.Name = "lbl_ida";
            this.lbl_ida.Size = new System.Drawing.Size(18, 20);
            this.lbl_ida.TabIndex = 23;
            this.lbl_ida.Text = "0";
            // 
            // txtbuscar2
            // 
            this.txtbuscar2.Location = new System.Drawing.Point(71, 44);
            this.txtbuscar2.Name = "txtbuscar2";
            this.txtbuscar2.Size = new System.Drawing.Size(383, 20);
            this.txtbuscar2.TabIndex = 22;
            this.txtbuscar2.TextChanged += new System.EventHandler(this.txtbuscar2_TextChanged);
            // 
            // dtg_asignacion
            // 
            this.dtg_asignacion.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtg_asignacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_asignacion.Location = new System.Drawing.Point(71, 314);
            this.dtg_asignacion.Name = "dtg_asignacion";
            this.dtg_asignacion.Size = new System.Drawing.Size(564, 150);
            this.dtg_asignacion.TabIndex = 21;
            this.dtg_asignacion.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_asignacion_CellDoubleClick);
            // 
            // btn_eliminarA
            // 
            this.btn_eliminarA.BackColor = System.Drawing.Color.White;
            this.btn_eliminarA.Image = ((System.Drawing.Image)(resources.GetObject("btn_eliminarA.Image")));
            this.btn_eliminarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_eliminarA.Location = new System.Drawing.Point(528, 266);
            this.btn_eliminarA.Name = "btn_eliminarA";
            this.btn_eliminarA.Size = new System.Drawing.Size(107, 32);
            this.btn_eliminarA.TabIndex = 20;
            this.btn_eliminarA.Text = "ELIMINAR";
            this.btn_eliminarA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_eliminarA.UseVisualStyleBackColor = false;
            // 
            // btn_cancelarA
            // 
            this.btn_cancelarA.BackColor = System.Drawing.Color.White;
            this.btn_cancelarA.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancelarA.Image")));
            this.btn_cancelarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelarA.Location = new System.Drawing.Point(528, 227);
            this.btn_cancelarA.Name = "btn_cancelarA";
            this.btn_cancelarA.Size = new System.Drawing.Size(107, 33);
            this.btn_cancelarA.TabIndex = 19;
            this.btn_cancelarA.Text = "CANCELAR";
            this.btn_cancelarA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelarA.UseVisualStyleBackColor = false;
            // 
            // btn_guardarA
            // 
            this.btn_guardarA.BackColor = System.Drawing.Color.White;
            this.btn_guardarA.Image = ((System.Drawing.Image)(resources.GetObject("btn_guardarA.Image")));
            this.btn_guardarA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_guardarA.Location = new System.Drawing.Point(528, 178);
            this.btn_guardarA.Name = "btn_guardarA";
            this.btn_guardarA.Size = new System.Drawing.Size(107, 43);
            this.btn_guardarA.TabIndex = 18;
            this.btn_guardarA.Text = "GUARDAR";
            this.btn_guardarA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_guardarA.UseVisualStyleBackColor = false;
            this.btn_guardarA.Click += new System.EventHandler(this.btn_guardarA_Click);
            // 
            // btn_nuevoA
            // 
            this.btn_nuevoA.BackColor = System.Drawing.Color.White;
            this.btn_nuevoA.Image = ((System.Drawing.Image)(resources.GetObject("btn_nuevoA.Image")));
            this.btn_nuevoA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_nuevoA.Location = new System.Drawing.Point(528, 133);
            this.btn_nuevoA.Name = "btn_nuevoA";
            this.btn_nuevoA.Size = new System.Drawing.Size(107, 37);
            this.btn_nuevoA.TabIndex = 17;
            this.btn_nuevoA.Text = "NUEVO";
            this.btn_nuevoA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_nuevoA.UseVisualStyleBackColor = false;
            this.btn_nuevoA.Click += new System.EventHandler(this.btn_nuevoA_Click);
            // 
            // txt_matricula
            // 
            this.txt_matricula.Location = new System.Drawing.Point(217, 135);
            this.txt_matricula.Name = "txt_matricula";
            this.txt_matricula.Size = new System.Drawing.Size(193, 20);
            this.txt_matricula.TabIndex = 16;
            // 
            // cmb_materia
            // 
            this.cmb_materia.FormattingEnabled = true;
            this.cmb_materia.Location = new System.Drawing.Point(217, 208);
            this.cmb_materia.Name = "cmb_materia";
            this.cmb_materia.Size = new System.Drawing.Size(193, 21);
            this.cmb_materia.TabIndex = 15;
            this.cmb_materia.Click += new System.EventHandler(this.cmb_materia_Click);
            // 
            // cmb_grupo
            // 
            this.cmb_grupo.FormattingEnabled = true;
            this.cmb_grupo.Location = new System.Drawing.Point(217, 171);
            this.cmb_grupo.Name = "cmb_grupo";
            this.cmb_grupo.Size = new System.Drawing.Size(193, 21);
            this.cmb_grupo.TabIndex = 14;
            this.cmb_grupo.SelectedIndexChanged += new System.EventHandler(this.cmb_grupo_SelectedIndexChanged);
            this.cmb_grupo.Click += new System.EventHandler(this.cmb_grupo_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.SkyBlue;
            this.label14.Location = new System.Drawing.Point(122, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "MATERIA";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.SkyBlue;
            this.label13.Location = new System.Drawing.Point(131, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 16);
            this.label13.TabIndex = 12;
            this.label13.Text = "GRUPO";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.SkyBlue;
            this.label12.Location = new System.Drawing.Point(108, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 16);
            this.label12.TabIndex = 11;
            this.label12.Text = "MATRICULA";
            // 
            // Frm_Profesor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1345, 617);
            this.Controls.Add(this.tabControl1);
            this.Name = "Frm_Profesor";
            this.Text = "Frm_Profesor";
            this.Load += new System.EventHandler(this.Frm_Profesor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_profesor)).EndInit();
            this.grp_estudios.ResumeLayout(false);
            this.grp_estudios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_estudios)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_asignacion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.Label lbl_nombre;
        private System.Windows.Forms.DataGridView dtg_profesor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_estado;
        private System.Windows.Forms.ComboBox cmd_ciudad;
        private System.Windows.Forms.TextBox txt_apellidom;
        private System.Windows.Forms.TextBox txt_apellidop;
        private System.Windows.Forms.TextBox txt_direccion;
        private System.Windows.Forms.TextBox txt_cedula;
        private System.Windows.Forms.TextBox txt_titulo;
        private System.Windows.Forms.Button Btn_Nuevo;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.Button Btn_cancelar;
        private System.Windows.Forms.Button Btn_eliminar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox grp_estudios;
        private System.Windows.Forms.DataGridView dtg_estudios;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.Button BtnEliminarMaestria;
        private System.Windows.Forms.Button BtnCargarMaestria;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_titupdf;
        private System.Windows.Forms.Label lbl_estudio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_eliminardoctorado;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btn_eliminartitulo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_doctpdf;
        private System.Windows.Forms.TextBox txt_maestriapdf;
        private System.Windows.Forms.DateTimePicker txt_fechanac;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtg_asignacion;
        private System.Windows.Forms.Button btn_eliminarA;
        private System.Windows.Forms.Button btn_cancelarA;
        private System.Windows.Forms.Button btn_guardarA;
        private System.Windows.Forms.Button btn_nuevoA;
        private System.Windows.Forms.TextBox txt_matricula;
        private System.Windows.Forms.ComboBox cmb_materia;
        private System.Windows.Forms.ComboBox cmb_grupo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtbuscar2;
        private System.Windows.Forms.Label lbl_ida;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}