﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class frm_EscuelaE : Form
    {
       
        
        private EscuelaEManejador _escuelaEManejador;
        private EscuelaE _escuelaE;
        private OpenFileDialog _imagenJpg;
        private string _ruta;
        public frm_EscuelaE()
        {
            InitializeComponent();
            _imagenJpg = new OpenFileDialog();
            
        }
        private void EscuelaE_Load(object sender, EventArgs e)
        {
            

            _escuelaEManejador = new EscuelaEManejador();
            _escuelaE = new EscuelaE();
            Buscarescuela("");
            Modificarescuela();
           //lbl_logo.Text = dtg_escuela.CurrentRow.Cells["logo"].Value.ToString();
            mostrarlogo();
           

        }
        private void guardarescuela()
        {
            _escuelaEManejador.guardar(_escuelaE);
        }
        private void Cargarescuela()//crear global el objeto para usarlo en otro lugar 
        {
            _escuelaE.No = Convert.ToInt32(lbl_id.Text);
            _escuelaE.Nombre = txt_nombre.Text;
            _escuelaE.RFC = txt_rfc.Text;
            _escuelaE.Domicilio = txt_domicilio.Text;
            _escuelaE.Telefono = Convert.ToInt32(txt_telefono.Text);
            _escuelaE.Correoe = txt_correoe.Text;
            _escuelaE.Pagina_w = txt_pagina.Text;
            _escuelaE.Director = txt_director.Text;
            _escuelaE.Logo = lbl_logo.Text;
        }
        private void Modificarescuelade()//funcion para  modificar  la tabla 
        {
           DataSet ds;
            ds = _escuelaEManejador.getescuelad();
            lbl_id.Text = ds.Tables[0].Rows[0]["no"].ToString();
            
            txt_nombre.Text = ds.Tables[0].Rows[0]["nombre"].ToString();
            txt_rfc.Text = ds.Tables[0].Rows[0]["RFC"].ToString();
            txt_domicilio.Text = ds.Tables[0].Rows[0]["domicilio"].ToString();
            txt_telefono.Text = ds.Tables[0].Rows[0]["telefono"].ToString();
            txt_correoe.Text = ds.Tables[0].Rows[0]["correoe"].ToString();
            txt_pagina.Text = ds.Tables[0].Rows[0]["pagina_w"].ToString();
            txt_director.Text = ds.Tables[0].Rows[0]["director"].ToString();
            lbl_logo.Text = ds.Tables[0].Rows[0]["logo"].ToString();

        }

        private void Buscarescuela(string filtro)//solo para data
        {
            dtg_escuela.DataSource = _escuelaEManejador.Getescuela(filtro);//pasar la lista 
        }
        private void btn_guardar_Click(object sender, EventArgs e)
        {
            
            Cargarescuela();

            //primero se carga el usuario 
            try
            {
                GuardarArchivodoctoradojpg();
                guardarescuela();//se lo pasamos a guardar usuario 

                caso1();
                Buscarescuela("");
                // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex +"");
            }
            cancelardatos();
        }
        private void caso1 ()
        {
            if (picturebx_logo.Image== null)
            {
                EliminarImgagenJpg();
            }

        }

        private void dtg_escuela_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtg_escuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lbl_id.Text = "1";
            btn_cancelar.Enabled = true;
            try
            {
                Modificarescuela();
                Buscarescuela("");
                //picturebx_logo.Image = Image.FromFile(@"C:\Users\jesy\Documents\5 SEMESTRE\TALLER DE BD\1 PARCIAL\ControlEscolar\ControlEscolar\bin\Debug\" + txt_nombre.Text + "\\" + lbl_logo.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       

        private void btn_cragar_Click(object sender, EventArgs e)
        {
            
            CargarImagenJpg();

            
        }
        private void CargarImagenJpg()
        {
            _imagenJpg.Filter = "Imagen tipo(*.jpg)|*.jpg"; //extencion de la imagen
            _imagenJpg.Title = "cargar imagen";
            _imagenJpg.ShowDialog();


            if (_imagenJpg.FileName != "")

            {
                FileInfo file = new FileInfo(_imagenJpg.FileName);
                var f = file.Length / 1000;
                if (f <= 5)
                {
                    picturebx_logo.ImageLocation = _imagenJpg.FileName;

                    var archivo = new FileInfo(_imagenJpg.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                    lbl_logo.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;
                }
                else
                { 
                    MessageBox.Show(" No se permiten imagenes mayores de 5 megas ");
                }
            }

        }
        private void GuardarImagenJpg()
        {
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivo = new FileInfo(_imagenJpg.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                picturebx_logo.Image = null;
                                lbl_logo.Text = "";
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name, true); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }
        private void GuardarArchivodoctoradojpg()
        {
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivo = new FileInfo(_imagenJpg.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name, true);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name, true); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }
        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            
            picturebx_logo.Image = null;
            lbl_logo.Text = "";

        }
        private void EliminarImgagenJpg()
        {
           
                if (Directory.Exists(_ruta)) // exixtente del archivo
                {
                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg"); //devuelve un arreglo 

                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar iamgen 
                        archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                        }
                    }
                }

            
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            Modificarescuela();
            if (MessageBox.Show("Estas seguro que deseas cancelar ", "Cancelar Operacion", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    lbl_logo.Text = dtg_escuela.CurrentRow.Cells["logo"].Value.ToString();
                    picturebx_logo.ImageLocation = @"C:\Users\jesy\Documents\5 SEMESTRE\TALLER DE BD\1 PARCIAL\ControlEscolar\ControlEscolar\bin\Debug\" + txt_nombre.Text + "\\" + lbl_logo.Text;
                   
                    
                    cancelardatos();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
           
            
        }

        private void gb_logo_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            ACTUALIZAR();
            string v = txt_nombre.Text;
            _ruta = Application.StartupPath + "//" + v + "//";
        }
        private void ACTUALIZAR()
        {
            txt_correoe.Enabled = true;
            txt_director.Enabled = true;
            txt_domicilio.Enabled = true;
            txt_nombre.Enabled = true;
            txt_pagina.Enabled = true;
            txt_rfc.Enabled = true;
            txt_telefono.Enabled = true;

           
        }

        private void cancelardatos()
        {
            txt_correoe.Enabled = false;
            txt_director.Enabled = false;
            txt_domicilio.Enabled = false;
            txt_nombre.Enabled = false;
            txt_pagina.Enabled = false;
            txt_rfc.Enabled = false;
            txt_telefono.Enabled = false;
            btn_eliminar.Enabled = false;
            btn_cragar.Enabled = false;


        }
        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            ACTUALIZAR();
            string v = txt_nombre.Text;
            _ruta = Application.StartupPath + "//" + v + "//";
            btn_guardar.Enabled = true;
            btn_cancelar.Enabled = true;
            btn_cragar.Enabled = true;
            btn_eliminar.Enabled = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            GuardarArchivodoctoradojpg();
        }


        private void mostrarlogo()
        {

            if (lbl_logo.Text != "")
            {

                picturebx_logo.ImageLocation = (@"C:\Users\jesy\Documents\5 SEMESTRE\TALLER DE BD\1 PARCIAL\ControlEscolar\ControlEscolar\bin\Debug\" + txt_nombre.Text + "\\" + lbl_logo.Text);
            }
            
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            //picturebx_logo.Image = Image.FromFile(@"C:\Users\jesy\Documents\5 SEMESTRE\TALLER DE BD\1 PARCIAL\ControlEscolar\ControlEscolar\bin\Debug\" + txt_nombre.Text + "\\" + lbl_logo.Text);

        }
        private void Modificarescuela()//funcion para  modificar  la tabla 
        {
            /*DataSet ds;
            ds = _escuelaEManejador.getescuelad();
            lbl_id.Text = ds.Tables[0].Rows[0]["no"].ToString();*/
            lbl_id.Text = dtg_escuela.CurrentRow.Cells["no"].Value.ToString();
            txt_nombre.Text = dtg_escuela.CurrentRow.Cells["nombre"].Value.ToString();
            txt_rfc.Text = dtg_escuela.CurrentRow.Cells["RFC"].Value.ToString();
            txt_domicilio.Text = dtg_escuela.CurrentRow.Cells["domicilio"].Value.ToString();
            txt_telefono.Text = dtg_escuela.CurrentRow.Cells["telefono"].Value.ToString();
            txt_correoe.Text = dtg_escuela.CurrentRow.Cells["correoe"].Value.ToString();
            txt_pagina.Text = dtg_escuela.CurrentRow.Cells["pagina_w"].Value.ToString();
            txt_director.Text = dtg_escuela.CurrentRow.Cells["director"].Value.ToString();
            lbl_logo.Text = dtg_escuela.CurrentRow.Cells["logo"].Value.ToString();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ACTUALIZAR();
            string v = txt_nombre.Text;
            _ruta = Application.StartupPath + "//" + v + "//";
            btn_guardar.Enabled = true;
            btn_cancelar.Enabled = true;
            btn_cragar.Enabled = true;
            btn_eliminar.Enabled = true;

        }
    }
}

