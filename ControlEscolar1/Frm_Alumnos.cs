﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;// acceder a entidades y Logica de Negocios
using LogicaNegocios.ControlEscolar;
using Microsoft.Office.Interop.Excel;

namespace ControlEscolar
{
    public partial class Frm_Alumnos : Form
    {
        public AlumnosManejador _alumnosManejador;//Instancia de la clase Usuario manejador
        private EstadosManejador _estadoManejador;//Instancia de la clase Estado manejador
        private CiudadesManejador _ciudadesManejador;//Instancia de la clase Ciudades manejador
        private Alumnos _alumnos; //HACER UN OBJETO TIPO ALUMNO
        public Frm_Alumnos()
        {
            InitializeComponent();
            _alumnosManejador = new AlumnosManejador();
            _estadoManejador = new EstadosManejador();
            _ciudadesManejador = new CiudadesManejador();
            _alumnos = new Alumnos();// para validar

        }
        public void Frm_Alumnos_Load(object sender, EventArgs e)
        {
            //buscar usuario cuando carge la presentacion
            BuscarAlumnos("");
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();


            //cmb_estado.DataSource = _alumnosManejador.Getestados("");
            //cmb_estado.DisplayMember = "Nombre";


        }
        private void BuscarAlumnos(string filtro)
        {
            dgv_alumnos.DataSource = _alumnosManejador.GetAlumnos(filtro);//pasar la lista 
        }

        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)//-1
        {
            Btn_Nuevo.Enabled = Nuevo;
            Btn_Eliminar.Enabled = eliminar;
            Btn_guardar.Enabled = guardar;
            Btn_Cancelar.Enabled = Cancelar;
        }
        private void ControlCuadros(bool activar)//0
        {
            txt_nombre.Enabled = activar;
            txt_apellidop.Enabled = activar;
            txt_apellidom.Enabled = activar;
            dtm_fechanac.Enabled = activar;
            txt_domicilio.Enabled = activar;
            txt_email.Enabled = activar;
            txt_sexo.Enabled = activar;
            Cmb_Ciudad.Enabled = activar;
            cmb_estado.Enabled = activar;
        }
        private void ModificarAlumno()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            lbl_id.Text = dgv_alumnos.CurrentRow.Cells["N_Control"].Value.ToString();//mayuscula del Get
            txt_nombre.Text = dgv_alumnos.CurrentRow.Cells["Nombre"].Value.ToString();
            txt_apellidop.Text = dgv_alumnos.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            txt_apellidom.Text = dgv_alumnos.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            dtm_fechanac.Text = dgv_alumnos.CurrentRow.Cells["Fecha_nac"].Value.ToString();
            txt_domicilio.Text = dgv_alumnos.CurrentRow.Cells["Domicilio"].Value.ToString();
            txt_email.Text = dgv_alumnos.CurrentRow.Cells["Email"].Value.ToString();
            txt_sexo.Text = dgv_alumnos.CurrentRow.Cells["Sexo"].Value.ToString();
            cmb_estado.Text = dgv_alumnos.CurrentRow.Cells["Cmb_Estado"].Value.ToString();
            Cmb_Ciudad.Text = dgv_alumnos.CurrentRow.Cells["Cmb_Ciudad"].Value.ToString();

        }
        private void limpiarCuadros()
        {
            lbl_id.Text = "0";
            txt_nombre.Text = "";
            txt_apellidop.Text = "";
            txt_apellidom.Text = "";
            dtm_fechanac.Text = "";
            txt_domicilio.Text = "";
            txt_email.Text = "";
            txt_sexo.Text = "";
            cmb_estado.Text = "";
            Cmb_Ciudad.Text = "";
        }

        private void EliminarAlumno()//necesitamos el id int para que elimine el dato
        {
            var N_Control = dgv_alumnos.CurrentRow.Cells["N_Control"].Value;//DEPENDE DEL DQATA
            _alumnosManejador.eliminar(Convert.ToInt32(N_Control));//EL METODO LO NECESITA INT
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlCuadros(true);
            txt_nombre.Focus();
        }
        //validar inicio
        private bool validarAlumno()
        {
            var tupla = _alumnosManejador.ValidarAlumno(_alumnos);// cachar la tupla
            var valido = tupla.Item1;//pasar
            var mensaje = tupla.Item2;//pasar

            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return valido;
        }
        private void Btn_guardar_Click(object sender, EventArgs e)
        {
            CargarAlumno();//primero se carga el usuario 

            if (validarAlumno())// control ks +if 
            {
                ControlBotones(true, false, false, true);
                ControlCuadros(false);
                try
                {

                    guardarAlumno();//se lo pasamos a guardar usuario 
                    limpiarCuadros();
                    BuscarAlumnos(""); // mandar llamar al data grip con los nuevos datos
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void guardarAlumno()
        {
            _alumnosManejador.guardar(_alumnos);//sacar el usuario para mandarlo llamar en otro lugar
        }
        private void CargarAlumno()//crear global el objeto para usarlo en otro lugar 
        {
            
            _alumnos.N_Control = Convert.ToInt32(lbl_id.Text);
            _alumnos.Nombre = txt_nombre.Text;
            _alumnos.Apellidopaterno = txt_apellidop.Text;
            _alumnos.Apellidomaterno = txt_apellidom.Text;
            _alumnos.Fecha_nac = dtm_fechanac.Text;
            _alumnos.Domicilio = txt_domicilio.Text;
            _alumnos.Email = txt_email.Text;
            _alumnos.Sexo = txt_sexo.Text;
        }

        //validar fin







        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txt_buscar.Text); //filtrar en la busqueda por letra 
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAlumno();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgv_alumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumno();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgv_alumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Cmb_Ciudad_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void TraerCiudades(string filtro)
        {
            Cmb_Ciudad.DataSource = _ciudadesManejador.GetCiudades(filtro);
            Cmb_Ciudad.DisplayMember = "Nombre";
        }

        private void TraerEstados(string filtro)
        {
            cmb_estado.DataSource = _estadoManejador.GetEstados(filtro);
            cmb_estado.DisplayMember = "Nombre";
            cmb_estado.ValueMember = "Codigo";

        }

        private void lblId_Click(object sender, EventArgs e)
        {

        }

        private void cmb_estado_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void cmb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades(cmb_estado.SelectedValue.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcell(dgv_alumnos);
        }

        private void ExportarDataGridViewExcell(DataGridView grd)
        {

            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //recorremos el data gried llenando la hoja de trabajo
                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                   Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }
            catch (Exception)
            {
                MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //CREAR UNA NUEVA FUNCION BOOL

        }
    }
}
