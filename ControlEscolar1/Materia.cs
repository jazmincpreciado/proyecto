﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;



namespace ControlEscolar
{
    public partial class Materia : Form
    {
        private MateriaRelManejador _materiaRelManejador;
        private materiaAManejador _materiaAManejador;
        private materia _materia;
        private void Materia_Load(object sender, EventArgs e)
        {
            
            _materiaRelManejador = new MateriaRelManejador();
            _materiaAManejador = new materiaAManejador();
            _materia = new materia();
            Buscarmateria("");
           

        }

        public Materia()
        {
            InitializeComponent();
        }
       
        private void Buscarmateria(string filtro)//solo para data
        {
            dtg_materias.DataSource = _materiaRelManejador.GetMateriaRel(filtro);//pasar la lista 
        }
        private void Traermateria(string filtro)//solo para combo
        {
            Cmb_materias.DataSource = _materiaAManejador.GetMaterias(filtro);
            Cmb_materias.DisplayMember = "nombreM";
            Cmb_materias.ValueMember = "no";

        }
        private void Traermaterias(string filtro)//solo para combo
        {
            Cmb_materiaF.DataSource = _materiaAManejador.GetMaterias(filtro);
            Cmb_materiaF.DisplayMember = "nombreM";
            Cmb_materiaF.ValueMember = "no";

        }

        private void Cmb_materias_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Cmb_materias_Click(object sender, EventArgs e)
        {
            Traermateria("");
        }

        private void Cmb_materiaF_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Cmb_materiaF_Click(object sender, EventArgs e)
        {
            Traermaterias("");
        }
        private void ModificarMateria()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            lbl_id.Text = dtg_materias.CurrentRow.Cells["id"].Value.ToString();//mayuscula del Get
            txt_nombre.Text = dtg_materias.CurrentRow.Cells["Materia1"].Value.ToString();
            txt_id.Text = dtg_materias.CurrentRow.Cells["Codigo"].Value.ToString();
            txt_horast.Text = dtg_materias.CurrentRow.Cells["Teoria"].Value.ToString();
            txt_horasp.Text = dtg_materias.CurrentRow.Cells["Practica"].Value.ToString();
            Cmb_materias.Text = dtg_materias.CurrentRow.Cells["Materia2"].Value.ToString();
            Cmb_materiaF.Text = dtg_materias.CurrentRow.Cells["Materia3"].Value.ToString();
        }
        
        
        
        private void guardarmateria()
        {
            _materiaAManejador.guardar(_materia);
        }
        private void Cargarmateria()//crear global el objeto para usarlo en otro lugar 
        {
            _materia.No = Convert.ToInt32(lbl_id.Text);
            _materia.NombreM = txt_nombre.Text;
            _materia.IdMA = txt_id.Text;
            _materia.HorastMA = Convert.ToInt32(txt_horast.Text);
            _materia.HorasprMA = Convert.ToInt32(txt_horasp.Text);
            _materia.Fk_materiaS = Cmb_materias.Text;
            _materia.Fk_materiaN =Cmb_materiaF.Text;
            Console.WriteLine("entro");

        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            
            Cargarmateria();
            //primero se carga el usuario 
          // pictureBox1.Visible = false;
            
           

            try
            {

                guardarmateria();//se lo pasamos a guardar usuario 
              
                Buscarmateria("");
                limpiarCuadros();// mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, true);
            ControlCuadros(true);
           txt_id.Focus();
            _materia.No = 0;

        }

        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)//-1
        {
            btn_nuevo.Enabled = Nuevo;
            
            btn_guardar.Enabled = guardar;
            btn_cancelar.Enabled = Cancelar;
        }
        private void ControlCuadros(bool activar)//0
        {
            txt_id.Enabled = activar;
            txt_nombre.Enabled = activar;
            txt_horasp.Enabled = activar;
            txt_horast.Enabled = activar;
            txt_id.Enabled = activar;
            txt_id.Enabled = activar;
            Cmb_materiaF.Enabled = activar;
            Cmb_materias.Enabled = activar;

        }
        private void Eliminarmateria()//necesitamos el id int para que elimine el dato
        {
            var no  = dtg_materias.CurrentRow.Cells["id"].Value;//DEPENDE DEL DQATA
            _materiaRelManejador.eliminar(Convert.ToInt32(no));//EL METODO LO NECESITA INT 
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminarmateria();
                    Buscarmateria(""); 
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void limpiarCuadros()
        {
            ;
            lbl_id.Text = "0";
            txt_id.Text = "";
            txt_nombre.Text = "";
           txt_horast.Text = "";
           txt_horasp.Text = "";
            Cmb_materias.Text = "";
            Cmb_materiaF.Text = "";

        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarmateria(txt_buscar.Text);
        }
        private void Eliminarmateria1()//necesitamos el id int para que elimine el dato
        {
            var no = dtg_materias.CurrentRow.Cells["no"].Value;//DEPENDE DEL DQATA
            _materiaAManejador.eliminar(Convert.ToInt32(no));//EL METODO LO NECESITA INT 
        }
        private void btn_elimanrmateria_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminarmateria1();
                  
                    Buscarmateria("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
           
            //pictureBox1.Visible = true;
        }

        private void dtg_materia_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dtg_materias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMateria();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dtg_materias_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtg_materia_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtg_materias_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtg_materias_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMateria();
                Buscarmateria("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txt_id_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }
    

