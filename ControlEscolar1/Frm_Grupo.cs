﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Grupo : Form
    {
        private GrupoManejador _grupoManejador;
        private Grupo _grupo;
        private AlumnosManejador _alumnosManejador;
        private VGrupoManejador _vGrupoManejador;

        public Frm_Grupo()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();//CONECTAR CON LOGICA DE NEGOCIOS (HACER INSTANCIA)
            _grupo = new Grupo();
            _alumnosManejador = new AlumnosManejador ();
            _vGrupoManejador = new VGrupoManejador();
        }
        private void Cargargrupo()//crear global el objeto para usarlo en otro lugar 
        {
            _grupo.Id=Convert.ToInt32(lbl_id.Text);

            _grupo.Nombre = Txt_nombre.Text;

            DataSet dAL;
            dAL = _alumnosManejador.getidalumno(Cmb_alumno.Text);
           _grupo.Fk_alumno = Convert.ToInt32(dAL.Tables[0].Rows[0]["N_Control"].ToString());

            //  _grupo.Fk_alumno = Convert.ToInt32(Cmb_alumno.SelectedValue);

        }




        private void Frm_Grupo_Load(object sender, EventArgs e)
        {
            Buscagrupo("");

        }
        private void Buscagrupo(string filtro)//cp
        {
            DGV_Grupo.DataSource =_vGrupoManejador.GetVGrupo(filtro);//pasar la lista 
        }
        private void Traeralumno(string filtro)//solo para combo
        {
            Cmb_alumno.DataSource = _alumnosManejador.GetAlumnos(filtro);
            Cmb_alumno.DisplayMember = "nombre";
            Cmb_alumno.ValueMember = "n_Control";
        }

        private void Cmb_alumno_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void Cmb_alumno_Click(object sender, EventArgs e)
        {
            Traeralumno("");
        }
        private void Eliminargrupo()//necesitamos el id int para que elimine el dato
        {
            var id= DGV_Grupo.CurrentRow.Cells["id"].Value;//DEPENDE DEL DQATA
            _grupoManejador.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT 
        }

        private void ModificarGrupo()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);
            lbl_id.Text = DGV_Grupo.CurrentRow.Cells["id"].Value.ToString();//mayuscula del Get
            Txt_nombre.Text = DGV_Grupo.CurrentRow.Cells["Grupo"].Value.ToString();//mayuscula del Ge
            Cmb_alumno.Text = DGV_Grupo.CurrentRow.Cells["Nombre1"].Value.ToString();
        }

        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)
        {
            btn_nuevo.Enabled = Nuevo;

            btn_guardar.Enabled = guardar;
            btn_cancelar.Enabled = Cancelar;
        }

        private void ControlCuadros(bool activar)
        {
            Txt_nombre.Enabled = activar;
            Cmb_alumno.Enabled= activar;       
       
        }
        private void guardargrupo()
        {
            _grupoManejador.guardar(_grupo);
        }
        private void btn_guardar_Click(object sender, EventArgs e)
        {
            Cargargrupo ();
            ControlBotones(true, false, false, true);
            ControlCuadros(true);
            //primero se carga el usuario 
            try
            {
             
               guardargrupo();//se lo pasamos a guardar usuario 

                limpiarCuadros();
               
                Buscagrupo("");
                // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex + "");
            }

        }
       

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void limpiarCuadros()
        {
            Txt_nombre.Text= "";
            Cmb_alumno.Text = "";

        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminargrupo();
                    Buscagrupo("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DGV_Grupo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DGV_Grupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupo();
                Buscagrupo("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cmb_alumno_DropDownStyleChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscagrupo(TXT_BUSCAR.Text);
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlCuadros(true);

        }
    }
    
}
