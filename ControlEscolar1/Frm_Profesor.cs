﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Profesor : Form
    {
        public static string v;
        private ProfesorManejador _profesorManejador;
        private EstadosManejador _estadoManejador;//Instancia de la clase Estado manejador
        private CiudadesManejador _ciudadesManejador;//Instancia de la clase Ciudades manejador
        private Profesor _profesor;
        private estudios _estudios;
        private EstudioManejador _estudioManejador;
        private OpenFileDialog _archivoTituloPdf;
        private OpenFileDialog _archivoMaestriaPdf;
        private OpenFileDialog _archivoDoctoradoPdf;
        private string _ruta;
        private GrupoManejador _grupoManejador;
        private Grupo _grupo;
        private materiaAManejador _materiaAManejador;
        private materia _materia;
        private AsignacionManejador _asignacionManejador;
        private Asignacion _asignacion;
        private Vasignacion _vasignacion;
        private VAsignacionManejador _vAsignacionManejador;
        public Frm_Profesor()
        {
            
            InitializeComponent();
            _profesorManejador = new ProfesorManejador();
            _estadoManejador = new EstadosManejador();
            _ciudadesManejador = new CiudadesManejador();
            _profesor = new Profesor();
            _estudioManejador = new EstudioManejador();
             _estudios =new estudios();
            _grupoManejador = new GrupoManejador();
            _grupo = new Grupo();
            _materiaAManejador = new materiaAManejador();
            _materia = new materia();
            _asignacion = new Asignacion();
            _asignacionManejador = new AsignacionManejador();

            _vasignacion = new Vasignacion();
            _vAsignacionManejador = new VAsignacionManejador();

            _archivoTituloPdf = new OpenFileDialog();
            _archivoMaestriaPdf = new OpenFileDialog();
            _archivoDoctoradoPdf = new OpenFileDialog();
            
        }
        private void Frm_Profesor_Load(object sender, EventArgs e)
        {
            BuscarPofesor("");
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
            Buscarestudio("");
            BuscarAsignacion("");
        }
        private void BuscarPofesor(string filtro)
        {
            dtg_profesor.DataSource = _profesorManejador.GetProfesor(filtro);//pasar la lista 
        }
        private void  BuscarAsignacion(string filtro)
        {
            dtg_asignacion.DataSource = _vAsignacionManejador.GetVasignacion(filtro);
        }
        private void Buscarestudio(string filtro)
        {
            dtg_estudios.DataSource = _estudioManejador.GetEstudios(txt_nombre.Text);
        }
        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)//-1
        {
            Btn_Nuevo.Enabled = Nuevo;
            Btn_Guardar.Enabled = guardar;
            Btn_cancelar.Enabled = Cancelar;
            Btn_eliminar .Enabled = eliminar;
            
        }
        private void ControlBotonesAsignacion(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)
        {
            btn_nuevoA.Enabled = Nuevo;
            btn_eliminarA.Enabled = eliminar;
            btn_cancelarA.Enabled = Cancelar;
            btn_guardarA.Enabled = guardar;
        }


        private void ControlCuadros(bool activar)//0
        {
            txt_nombre.Enabled = activar;
            txt_apellidop.Enabled = activar;
            txt_apellidom.Enabled = activar;
           txt_direccion.Enabled = activar;
            txt_titulo.Enabled = activar;
           txt_cedula.Enabled = activar;
            cmb_estado.Enabled = activar;
            cmd_ciudad.Enabled = activar;
            txt_fechanac.Enabled = activar;
            
        }
        private void ControlCuadrosAsignacion(bool activar)
        {
            txt_matricula.Enabled = activar;
            cmb_grupo.Enabled = activar;
            cmb_materia.Enabled = activar;
        }

        private void ModificarProfesor()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            _profesor.No_control = dtg_profesor.CurrentRow.Cells["No_control"].Value.ToString();
            txt_nombre.Text = dtg_profesor.CurrentRow.Cells["nombre"].Value.ToString();
            txt_apellidom.Text = dtg_profesor.CurrentRow.Cells["apellido_M"].Value.ToString();
            txt_apellidop.Text = dtg_profesor.CurrentRow.Cells["apellido_P"].Value.ToString();
            txt_direccion.Text = dtg_profesor.CurrentRow.Cells["direccion"].Value.ToString();
            cmb_estado .Text = dtg_profesor.CurrentRow.Cells["estado"].Value.ToString();
            cmd_ciudad.Text = dtg_profesor.CurrentRow.Cells["ciudad"].Value.ToString();
            txt_cedula.Text = dtg_profesor.CurrentRow.Cells["n_cedula"].Value.ToString();
            txt_titulo.Text = dtg_profesor.CurrentRow.Cells["titulo"].Value.ToString();
            txt_fechanac.Text = dtg_profesor.CurrentRow.Cells["fecha_nac"].Value.ToString();
            
        }
        private void ModificarAsignacion()
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            lbl_ida.Text = dtg_asignacion.CurrentRow.Cells["id"].Value.ToString();//mayuscula del Get
            txt_matricula.Text= dtg_asignacion.CurrentRow.Cells["Matricula"].Value.ToString();
            cmb_grupo.Text= dtg_asignacion.CurrentRow.Cells["Grupo"].Value.ToString();
            cmb_materia.Text= dtg_asignacion.CurrentRow.Cells["Materia"].Value.ToString();

        }

        private void limpiarCuadros()
        {
            lbl_id.Text = "";
            txt_nombre.Text = "";
            txt_apellidop.Text = "";
            txt_apellidom.Text = "";
            cmb_estado.Text = "";
            cmd_ciudad.Text = "";
            txt_direccion.Text = "";
            txt_cedula.Text=  "";
            txt_titulo.Text = "";
            txt_fechanac.Text = "";
        }
        private void LimpiarCuadrosAsignacion()
        {
            lbl_ida.Text = "0";
            txt_matricula.Text = "";
            cmb_grupo.Text = "";
            cmb_materia.Text = "";
        }
        private void EliminarProfesores()//necesitamos el id int para que elimine el dato
        {
            var N_Control = dtg_profesor.CurrentRow.Cells["No_Control"].Value;//DEPENDE DEL DQATA
            _profesorManejador.eliminar(N_Control.ToString());//EL METODO LO NECESITA INT 
        }
        private void EliminarAsignacion()
        {
            var id = dtg_asignacion.CurrentRow.Cells["id"].Value;//DEPENDE DEL DQATA
            _asignacionManejador.eliminara(Convert.ToInt32(id.ToString()));//EL METODO LO NECESITA INT 
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlCuadros(true);
            txt_nombre.Focus();
            _profesor.No_control = "";
            
        }
        private void btn_nuevoA_Click(object sender, EventArgs e)
        {
            ControlBotonesAsignacion(false, true, true, false);
            ControlCuadrosAsignacion(true);
            txt_matricula.Focus();
            
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            CargarProfesor();
            //primero se carga el usuario 

            ControlBotones(true, false, false, true);
                ControlCuadros(false);
                try
                {

                guardarProfesor();//se lo pasamos a guardar usuario 
                    limpiarCuadros();
                    BuscarPofesor(""); // mandar llamar al data grip con los nuevos datos
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
          

        }
        private void btn_guardarA_Click(object sender, EventArgs e)
        {
            CargarAsignacion();
            //primero se carga el usuario 

            ControlBotonesAsignacion(true, false, false, true);
            ControlCuadrosAsignacion(true);
            try
            {

                guardarAsignacion();//se lo pasamos a guardar usuario 
                LimpiarCuadrosAsignacion();
                BuscarAsignacion(""); // mandar llamar al data grip con los nuevos datos
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void guardarProfesor()
        {
            _profesorManejador.guardar(_profesor);//sacar el usuario para mandarlo llamar en otro lugar
        }
        private void guardarAsignacion()
        {
            _asignacionManejador.guardara(_asignacion);
        }

        private void CargarProfesor()//crear global el objeto para usarlo en otro lugar 
        {
          
           //  _profesor.No_control = "";
            _profesor.Nombre = txt_nombre.Text;
            _profesor.Apellido_P = txt_apellidop.Text;
            _profesor.Apellido_M = txt_apellidom.Text;
            _profesor.Direccion = txt_direccion.Text;
            _profesor.Estado = cmb_estado.Text;
            _profesor.Ciudad = cmd_ciudad.Text;
            _profesor.N_cedula = Convert.ToInt32(txt_cedula.Text);
            _profesor.Titulo = txt_titulo.Text;
            _profesor.Fecha_nac = txt_fechanac.Text;
         
           

        }
        private void CargarAsignacion()
        {
            _asignacion.Id = Convert.ToInt32(lbl_ida.Text);
            _asignacion.Fk_profesor = txt_matricula.Text;

            DataSet ds;
            ds = _grupoManejador.getidgrupo(cmb_grupo.Text);
            _asignacion.Fk_grupo = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());

            //_asignacion.Fk_grupo = Convert.ToInt32(cmb_grupo.SelectedValue);
            //_asignacion.Fk_materia=cmb_materia.Text;

            DataSet dt;
           dt= _materiaAManejador.getidMATERIA(cmb_materia.Text);
            _asignacion.Fk_materia = dt.Tables[0].Rows[0]["idMA"].ToString();
        }
        private void guardarestudio()
        {
            _estudioManejador.guardar(_estudios);//sacar el usuario para mandarlo llamar en otro lugar
        }
        private void cargarEstudios()
        {

            _estudios.Id = Convert.ToInt32(lbl_estudio.Text);

            _estudios.Fk_nombre = txt_nombre.Text;
            _estudios.Titulo = txt_titupdf.Text;
            _estudios.Maestria = txt_maestriapdf.Text;
            _estudios.Doctorado = txt_doctpdf.Text;
        }
        private void TraerGrupo(string filtro)
        {
            cmb_grupo.DataSource = _grupoManejador.GetGrupon(filtro);
            cmb_grupo.DisplayMember = "nombre";
           
        }
        private void TraerMateria(string filtro)
        {
            cmb_materia.DataSource = _materiaAManejador.GetMaterias(filtro);
            cmb_materia.DisplayMember = "nombreM";
        }

        private void TraerCiudades(string filtro)
        {
           cmd_ciudad.DataSource = _ciudadesManejador.GetCiudades(filtro);
            cmd_ciudad.DisplayMember = "Nombre";
        }

        private void TraerEstados(string filtro)
        {
            cmb_estado.DataSource = _estadoManejador.GetEstados(filtro);
            cmb_estado.DisplayMember = "Nombre";
            cmb_estado.ValueMember = "Codigo";

        }
        private void cmb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades(cmb_estado.SelectedValue.ToString());
        }

        private void cmb_estado_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }
        private void cmb_grupo_Click(object sender, EventArgs e)
        {
            TraerGrupo("");
        }

        private void cmb_materia_Click(object sender, EventArgs e)
        {
            TraerMateria("");
        }
        
        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
           BuscarPofesor(txt_buscar.Text); //filtrar en la busqueda por letra   

        }
        private void txtbuscar2_TextChanged(object sender, EventArgs e)
        {
            BuscarAsignacion(txtbuscar2.Text);
        }
        private void cmb_grupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TraerMateria()
        }

        private void dtg_profesor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            grp_estudios.Visible = false;
            try
            {
                ModificarProfesor();
                BuscarPofesor("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarProfesores();
                    BuscarPofesor("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

           grp_estudios.Visible = true;
            Buscarestudio("");
            
            string v = txt_nombre.Text;
            _ruta = Application.StartupPath + "//" + v + "//";
            txt_doctpdf.Text = "";
            txt_titupdf.Text = "";
            txt_maestriapdf.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            cargarEstudios();
            guardarestudio();
            Buscarestudio("");
            GuardarArchivotituloPdf();
            GuardarArchivomaestriaPdf();
            GuardarArchivodoctoradoPdf();

        }

        private void txt_idp_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtg_profesor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            grp_estudios.Visible = false;
        }

        private void grp_estudios_Enter(object sender, EventArgs e)
        {

        }
        private void CargarArchivomaestriaPdf()
        {
            _archivoMaestriaPdf.Filter = "Archivo tipo(*.pdf) | *.pdf";
            _archivoMaestriaPdf.Title = "cargar archivo";
            _archivoMaestriaPdf.ShowDialog();

            if (_archivoMaestriaPdf.FileName != "")
            {
                var archivo = new FileInfo(_archivoMaestriaPdf.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                txt_maestriapdf.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;

            }
        }
            private void BtnCargarMaestria_Click(object sender, EventArgs e)
        {
            CargarArchivomaestriaPdf();
        }
        private void CargarArchivotituloPdf()
        {
            _archivoTituloPdf.Filter = "Archivo tipo(*.pdf) | *.pdf";
            _archivoTituloPdf.Title = "cargar archivo";
            _archivoTituloPdf.ShowDialog();

            if (_archivoTituloPdf.FileName != "")
            {
                var archivo = new FileInfo(_archivoTituloPdf.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                txt_titupdf.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;

            }

        }
        private void CargarArchivodoctoradoPdf()
        {
            _archivoDoctoradoPdf.Filter = "Archivo tipo(*.pdf) | *.pdf";
            _archivoDoctoradoPdf.Title = "cargar archivo";
            _archivoDoctoradoPdf.ShowDialog();

            if (_archivoDoctoradoPdf.FileName != "")
            {
                var archivo = new FileInfo(_archivoDoctoradoPdf.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                txt_doctpdf.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;

            }
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            CargarArchivotituloPdf();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CargarArchivodoctoradoPdf();
        }
        private void GuardarArchivomaestriaPdf()
        {
            if (_archivoMaestriaPdf.FileName != null)
            {
                if (_archivoMaestriaPdf.FileName != "")
                {
                    var archivo = new FileInfo(_archivoMaestriaPdf.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length == 2)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }
        private void GuardarArchivodoctoradoPdf()
        {
            if (_archivoDoctoradoPdf.FileName != null)
            {
                if (_archivoDoctoradoPdf.FileName != "")
                {
                    var archivo = new FileInfo(_archivoDoctoradoPdf.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length == 3)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name,true);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name,true); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }
        private void GuardarArchivotituloPdf()
        {
            if (_archivoTituloPdf.FileName != null)
            {
                if (_archivoTituloPdf.FileName != "")
                {
                    var archivo = new FileInfo(_archivoTituloPdf.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name,true); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }

        private void btn_eliminartitulo_Click(object sender, EventArgs e)
        {
            EliminarArchivotitulo();
        }
        private void EliminarArchivotitulo()
        {
            if (MessageBox.Show("estas seguro que deseas eliminar el archivo maestria", "eliminar Archivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (_archivoTituloPdf.FileName != null)
                {
                    if (_archivoTituloPdf.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoTituloPdf.FileName);
                        if (Directory.Exists(_ruta)) // exixtente del archivo
                        {
                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length ==3)
                            {
                                //codigo para remplazar iamgen 
                                archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();

                                }

                            }
                        }
                    }
                }
            }
        }

        private void BtnEliminarMaestria_Click(object sender, EventArgs e)
        {
            EliminarArchivomaestria();
        }
        private void EliminarArchivomaestria()
        {
            if (MessageBox.Show("estas seguro que deseas eliminar el archivo maestria", "eliminar Archivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (_archivoMaestriaPdf.FileName != null)
                {
                    if (_archivoMaestriaPdf.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoMaestriaPdf.FileName);
                        if (Directory.Exists(_ruta)) // exixtente del archivo
                        {
                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length ==2)
                            {
                                //codigo para remplazar iamgen 
                                archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();

                                }

                            }
                        }
                    }
                }
            }
        }

        private void btn_eliminardoctorado_Click(object sender, EventArgs e)
        {
            EliminarArchivodoctorado();
        }
        private void EliminarArchivodoctorado()
        {
            if (MessageBox.Show("estas seguro que deseas eliminar el archivo doctorado", "eliminar Archivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (_archivoDoctoradoPdf.FileName != null)
                {
                    if (_archivoDoctoradoPdf.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoDoctoradoPdf.FileName);
                        if (Directory.Exists(_ruta)) // exixtente del archivo
                        {
                            var obtenerArchivos = Directory.GetFiles(_ruta, "*."); //devuelve un arreglo 

                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length !=0)
                            {
                                //codigo para remplazar iamgen 
                                archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();

                                }

                            }
                        }
                    }
                }
            }
        }

        private void dtg_estudios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void dtg_asignacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, true);
            try
            {
                ModificarAsignacion();
               BuscarAsignacion("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

}
