﻿namespace ControlEscolar
{
    partial class FM_usuarios
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dgv_Usuarios = new System.Windows.Forms.DataGridView();
            this.lbl_nombre = new System.Windows.Forms.Label();
            this.lbl_apellidopaterno = new System.Windows.Forms.Label();
            this.lbl_apellidomaterno = new System.Windows.Forms.Label();
            this.lbl_contrasenia = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.txt_apellidopaterno = new System.Windows.Forms.TextBox();
            this.txt_apellidomaterno = new System.Windows.Forms.TextBox();
            this.txt_contrasenia = new System.Windows.Forms.TextBox();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.lbl_id = new System.Windows.Forms.Label();
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.lbl_buscar = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_nuevo = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Usuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Dgv_Usuarios
            // 
            this.Dgv_Usuarios.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.Dgv_Usuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_Usuarios.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.Dgv_Usuarios.Location = new System.Drawing.Point(27, 283);
            this.Dgv_Usuarios.Name = "Dgv_Usuarios";
            this.Dgv_Usuarios.Size = new System.Drawing.Size(547, 214);
            this.Dgv_Usuarios.TabIndex = 0;
            this.Dgv_Usuarios.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_Usuarios_CellDoubleClick);
            // 
            // lbl_nombre
            // 
            this.lbl_nombre.AutoSize = true;
            this.lbl_nombre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_nombre.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nombre.Location = new System.Drawing.Point(24, 73);
            this.lbl_nombre.Name = "lbl_nombre";
            this.lbl_nombre.Size = new System.Drawing.Size(61, 14);
            this.lbl_nombre.TabIndex = 1;
            this.lbl_nombre.Text = "NOMBRE";
            // 
            // lbl_apellidopaterno
            // 
            this.lbl_apellidopaterno.AutoSize = true;
            this.lbl_apellidopaterno.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_apellidopaterno.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_apellidopaterno.Location = new System.Drawing.Point(24, 121);
            this.lbl_apellidopaterno.Name = "lbl_apellidopaterno";
            this.lbl_apellidopaterno.Size = new System.Drawing.Size(151, 14);
            this.lbl_apellidopaterno.TabIndex = 2;
            this.lbl_apellidopaterno.Text = "APELLIDO PATERNO";
            // 
            // lbl_apellidomaterno
            // 
            this.lbl_apellidomaterno.AutoSize = true;
            this.lbl_apellidomaterno.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_apellidomaterno.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_apellidomaterno.Location = new System.Drawing.Point(24, 162);
            this.lbl_apellidomaterno.Name = "lbl_apellidomaterno";
            this.lbl_apellidomaterno.Size = new System.Drawing.Size(151, 14);
            this.lbl_apellidomaterno.TabIndex = 3;
            this.lbl_apellidomaterno.Text = "APELLIDO MATERNO";
            // 
            // lbl_contrasenia
            // 
            this.lbl_contrasenia.AutoSize = true;
            this.lbl_contrasenia.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_contrasenia.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contrasenia.Location = new System.Drawing.Point(24, 210);
            this.lbl_contrasenia.Name = "lbl_contrasenia";
            this.lbl_contrasenia.Size = new System.Drawing.Size(97, 14);
            this.lbl_contrasenia.TabIndex = 4;
            this.lbl_contrasenia.Text = "CONTRASEÑA";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(27, 89);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(380, 20);
            this.txt_nombre.TabIndex = 1;
            // 
            // txt_apellidopaterno
            // 
            this.txt_apellidopaterno.Location = new System.Drawing.Point(27, 139);
            this.txt_apellidopaterno.Name = "txt_apellidopaterno";
            this.txt_apellidopaterno.Size = new System.Drawing.Size(380, 20);
            this.txt_apellidopaterno.TabIndex = 2;
            // 
            // txt_apellidomaterno
            // 
            this.txt_apellidomaterno.Location = new System.Drawing.Point(27, 178);
            this.txt_apellidomaterno.Name = "txt_apellidomaterno";
            this.txt_apellidomaterno.Size = new System.Drawing.Size(380, 20);
            this.txt_apellidomaterno.TabIndex = 3;
            // 
            // txt_contrasenia
            // 
            this.txt_contrasenia.Location = new System.Drawing.Point(27, 226);
            this.txt_contrasenia.Name = "txt_contrasenia";
            this.txt_contrasenia.Size = new System.Drawing.Size(380, 20);
            this.txt_contrasenia.TabIndex = 4;
            // 
            // btn_guardar
            // 
            this.btn_guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_guardar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            //this.btn_guardar.Image = global::ControlEscolar.Properties.Resources.Save_as;
            this.btn_guardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_guardar.Location = new System.Drawing.Point(455, 136);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(107, 35);
            this.btn_guardar.TabIndex = 10;
            this.btn_guardar.Tag = "R";
            this.btn_guardar.Text = "GUARDAR";
            this.btn_guardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancelar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;

            //this.btn_cancelar.Image = global::ControlEscolar.Properties.Resources.Cancel;
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(455, 178);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(107, 39);
            this.btn_cancelar.TabIndex = 11;
            this.btn_cancelar.Text = "CANCELAR";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_eliminar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            //this.btn_eliminar.Image = global::ControlEscolar.Properties.Resources.Delete_frame;
            this.btn_eliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_eliminar.Location = new System.Drawing.Point(455, 223);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(107, 41);
            this.btn_eliminar.TabIndex = 12;
            this.btn_eliminar.Text = "ELIMINAR";
            this.btn_eliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_eliminar.UseVisualStyleBackColor = true;
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.Location = new System.Drawing.Point(427, 9);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(19, 20);
            this.lbl_id.TabIndex = 13;
            this.lbl_id.Text = "0";
            this.lbl_id.Click += new System.EventHandler(this.lbl_id_Click);
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(27, 32);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(248, 20);
            this.txt_buscar.TabIndex = 14;
            this.txt_buscar.TextChanged += new System.EventHandler(this.txt_buscar_TextChanged);
            // 
            // lbl_buscar
            // 
            this.lbl_buscar.AutoSize = true;
            this.lbl_buscar.Location = new System.Drawing.Point(281, 35);
            this.lbl_buscar.Name = "lbl_buscar";
            this.lbl_buscar.Size = new System.Drawing.Size(0, 13);
            this.lbl_buscar.TabIndex = 15;
            this.lbl_buscar.Click += new System.EventHandler(this.lbl_buscar_Click);
            // 
            // pictureBox1
            // 
            //this.pictureBox1.Image = global::ControlEscolar.Properties.Resources.Search_folder;
            this.pictureBox1.Location = new System.Drawing.Point(287, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // btn_nuevo
            // 
            this.btn_nuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_nuevo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            //this.btn_nuevo.Image = global::ControlEscolar.Properties.Resources.New_imagelist;
            this.btn_nuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_nuevo.Location = new System.Drawing.Point(455, 89);
            this.btn_nuevo.Name = "btn_nuevo";
            this.btn_nuevo.Size = new System.Drawing.Size(107, 41);
            this.btn_nuevo.TabIndex = 9;
            this.btn_nuevo.Text = "NUEVO";
            this.btn_nuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_nuevo.UseVisualStyleBackColor = true;
            this.btn_nuevo.Click += new System.EventHandler(this.btn_nuevo_Click);
            // 
            // pictureBox2
            // 
            //this.pictureBox2.Image = global::ControlEscolar.Properties.Resources.usuario_microfono;
            this.pictureBox2.Location = new System.Drawing.Point(481, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(68, 67);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // FM_usuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 529);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_buscar);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.btn_nuevo);
            this.Controls.Add(this.txt_contrasenia);
            this.Controls.Add(this.txt_apellidomaterno);
            this.Controls.Add(this.txt_apellidopaterno);
            this.Controls.Add(this.txt_nombre);
            this.Controls.Add(this.lbl_contrasenia);
            this.Controls.Add(this.lbl_apellidomaterno);
            this.Controls.Add(this.lbl_apellidopaterno);
            this.Controls.Add(this.lbl_nombre);
            this.Controls.Add(this.Dgv_Usuarios);
            this.Name = "FM_usuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuaurios";
            this.Load += new System.EventHandler(this.FM_usuarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Usuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dgv_Usuarios;
        private System.Windows.Forms.Label lbl_nombre;
        private System.Windows.Forms.Label lbl_apellidopaterno;
        private System.Windows.Forms.Label lbl_apellidomaterno;
        private System.Windows.Forms.Label lbl_contrasenia;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.TextBox txt_apellidopaterno;
        private System.Windows.Forms.TextBox txt_apellidomaterno;
        private System.Windows.Forms.TextBox txt_contrasenia;
        private System.Windows.Forms.Button btn_nuevo;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.Label lbl_buscar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

