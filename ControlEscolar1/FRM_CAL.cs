﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;
using Microsoft.Office.Interop.Excel;


namespace ControlEscolar
{
    public partial class FRM_CAL : Form
    {
        bool terminado = true;
        private GrupoManejador _grupoManejador;
        private Grupo _grupo;

        private AlumnosManejador _alumnosManejador;
        private Alumnos _alumno;

        private materiaAManejador _mateiaAManejador;
        private Materia _materia;

        private CalificacionesManejador _calificacionesManejador;
        private Calificaciones _calificaciones;

        private AsignacionManejador _asignacionManejador;
        private VAsignacionManejador _vAsignacionManejador;

        private VGrupoManejador _vGrupoManejador;
        private VCalificacionesManejador _vCalificacionesManejador;

        public FRM_CAL()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();//CONECTAR CON LOGICA DE NEGOCIOS (HACER INSTANCIA)
            _grupo = new Grupo();

            _alumnosManejador = new AlumnosManejador();
            _alumno = new Alumnos();

            _calificaciones = new Calificaciones();
            _calificacionesManejador = new CalificacionesManejador();

            _mateiaAManejador = new materiaAManejador();
            _materia = new Materia() ;

            _asignacionManejador = new AsignacionManejador();
            _vAsignacionManejador = new VAsignacionManejador();

            _vGrupoManejador= new VGrupoManejador();
            _vCalificacionesManejador = new VCalificacionesManejador();

        }
        private void CargarCalificaciones()//crear global el objeto para usarlo en otro lugar 
        {
            _calificaciones.Id = Convert.ToInt32(lbl_id.Text);
            _calificaciones.Grupo = CMB_GRUPO.Text;
            _calificaciones.Materia = CMB_MATERIA.Text;
            _calificaciones.Alumno=  Convert.ToInt32(CMB_ALUMNO.Text);
            _calificaciones.Parcial1 = Convert.ToDouble(TXT_PARCIAL1.Text);
            _calificaciones.Parcial2 = Convert.ToDouble(TXT_PARCIAL2.Text);
            _calificaciones.Parcial3 = Convert.ToDouble(TXT_PARCIAL3.Text);
            _calificaciones.Parcial4 = Convert.ToDouble(TXT_PARCIAL4.Text);

        }
        private void FRM_CAL_Load(object sender, EventArgs e)
        {
            Buscarcalificaciones("");
        }

        private void Buscarcalificaciones(string filtro)
        {
            DTG_CAL.DataSource =_vCalificacionesManejador .GetVcalificaciones(filtro);
        }

        private void TraerAlumno(string filtro)//solo para combo
        {
          CMB_ALUMNO.DataSource =_alumnosManejador.GetAlumnos(filtro);
          CMB_ALUMNO.DisplayMember = "N_Control";
          CMB_ALUMNO.ValueMember = "N_Control";

        }

        private void Traergrupo2(string filtro)//solo para combo
        {
            CMB_GRUPO.DataSource = _grupoManejador.GetGrupon(filtro);
            CMB_GRUPO.DisplayMember = "nombre";
            CMB_GRUPO.ValueMember = "id";


        }
        private void Traeralumno2(string filtro)
        {
            CMB_ALUMNO.DataSource = _alumnosManejador.GetAlumnos(filtro);
            CMB_ALUMNO.DisplayMember = "N_Control";

        }


        private void Traergrupo(string filtro)
        {
            CMB_GRUPO.DataSource = _grupoManejador.GetGrupo(filtro);
            CMB_GRUPO.DisplayMember = "nombre";
            
        }

        private void TraerMateria(string filtro)
        {
            CMB_MATERIA.DataSource =_vAsignacionManejador.GetVasignacion(CMB_MATERIA.Text);
            CMB_MATERIA.DisplayMember = "Materia";
            
        }


        private void CMB_ALUMNO_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Traergrupo(CMB_ALUMNO.SelectedValue.ToString());
        }

        private void CMB_GRUPO_SelectedIndexChanged(object sender, EventArgs e)
        {
            Traeralumno2(CMB_GRUPO.SelectedValue.ToString());
            //TraerMateria("");
        }

        private void CMB_ALUMNO_Click(object sender, EventArgs e)
        {
            CMB_ALUMNO.DataSource =_vGrupoManejador.GetVGrupo(CMB_GRUPO.Text);
            CMB_ALUMNO.DisplayMember = "Matricula";
        }

        private void CMB_GRUPO_Click(object sender, EventArgs e)
        {
            Traergrupo2("");
        }

        private void CMB_MATERIA_Click(object sender, EventArgs e)
        {
            CMB_MATERIA.DataSource = _vAsignacionManejador.GetVasignacion(CMB_GRUPO.Text);
            CMB_MATERIA.DisplayMember = "Materia";
        }
        private void EliminarCalificaciones()//necesitamos el id int para que elimine el dato
        {
            var id = DTG_CAL.CurrentRow.Cells["id"].Value;//DEPENDE DEL DQATA
            _calificacionesManejador.eliminar(Convert.ToInt32(id));//EL METODO LO NECESITA INT 
        }
        private void ModificarCalificaciones()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);
            lbl_id.Text = DTG_CAL.CurrentRow.Cells["id"].Value.ToString();//mayuscula del Get
            CMB_GRUPO.Text = DTG_CAL.CurrentRow.Cells["Grupo"].Value.ToString();//mayuscula del Ge
            CMB_MATERIA.Text = DTG_CAL.CurrentRow.Cells["Materia"].Value.ToString();
            CMB_ALUMNO.Text = DTG_CAL.CurrentRow.Cells["IDALUMNO"].Value.ToString();



            TXT_PARCIAL1.Text = DTG_CAL.CurrentRow.Cells["parcial1"].Value.ToString();
            TXT_PARCIAL2.Text = DTG_CAL.CurrentRow.Cells["parcial2"].Value.ToString(); 
            TXT_PARCIAL3.Text = DTG_CAL.CurrentRow.Cells["parcial3"].Value.ToString();
            TXT_PARCIAL4.Text = DTG_CAL.CurrentRow.Cells["parcial4"].Value.ToString();


        }
        private void ControlCuadros(bool activar)
        {
            TXT_PARCIAL1.Enabled = activar;
            TXT_PARCIAL2.Enabled = activar;
            TXT_PARCIAL3.Enabled = activar;
            TXT_PARCIAL4.Enabled = activar;
            

            CMB_ALUMNO.Enabled = activar;
            CMB_GRUPO.Enabled = activar;
            CMB_MATERIA.Enabled = activar;

        }

        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)
        {
          btn_AGREGAR .Enabled = Nuevo;

            btn_guardar.Enabled = guardar;
            btn_cancelar.Enabled = Cancelar;
            btn_eliminar.Enabled = eliminar;
        }

        private void guardarCalificaciones()
        {
           _calificacionesManejador.guardar(_calificaciones);
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            
            CargarCalificaciones();
            ControlBotones(true, false, false, true);
            ControlCuadros(true);
            //primero se carga el usuario 
            try
                {

                    guardarCalificaciones();//se lo pasamos a guardar usuario 

                    limpiarCuadros();
                    Buscarcalificaciones("");
                    // mandar llamar al data grip con los nuevos datos
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex + "");
                }

            
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void limpiarCuadros()
        {
            TXT_PARCIAL1.Text = "";
            TXT_PARCIAL2.Text = "";
            TXT_PARCIAL3.Text = "";
            TXT_PARCIAL4.Text = "";

            CMB_ALUMNO.Text = "";
            CMB_GRUPO.Text = "";
            CMB_MATERIA.Text = "";


        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarCalificaciones();
                    Buscarcalificaciones("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DTG_CAL_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ControlCuadros(true);
            try
            {
                ModificarCalificaciones();
                Buscarcalificaciones("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_AGREGAR_Click(object sender, EventArgs e)
        {
            ControlCuadros(true);
        }

        private void btn_exportar_Click(object sender, EventArgs e)
        {
            /*if(!bgvgExportar.IsBusy)

            {
                bgvgExportar.RunWorkerAsync();


            }*/
            ExportarDataGridViewExcell(DTG_CAL);

            
        }

        private void PROCESOCARGAR()
        {

            //for(inti=0;i<
            //Thread.Sleep(500);
        }

        private bool ExportarDataGridViewExcell(DataGridView grd)
        {

            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //recorremos el data gried llenando la hoja de trabajo
                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                   Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();

                    MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }

                return true;
            }
            catch (Exception)
            {
                
                return false;
            }

            //CREAR UNA NUEVA FUNCION BOOL

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Buscarcalificaciones(txt_buscar.Text);
        }

        private void CMB_ALUMNO_ChangeUICues(object sender, UICuesEventArgs e)
        {

        }

        private void TXT_PARCIAL1_Click(object sender, EventArgs e)
        {
            TXT_PARCIAL1.Text = "";
        }

        private void CMB_GRUPO_ChangeUICues(object sender, UICuesEventArgs e)
        {

        }

        private void bgvgExportar_DoWork(object sender, DoWorkEventArgs e)
        {
            terminado=ExportarDataGridViewExcell(DTG_CAL);
        }

        private void bgvgExportar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           if (terminado)
            { 
                MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
           else
            {
                MessageBox.Show("Reporte fallado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }

}
