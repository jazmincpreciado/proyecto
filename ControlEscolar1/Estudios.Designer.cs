﻿namespace ControlEscolar
{
    partial class Estudios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Estudios));
            this.txt_maestria = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_doctorado = new System.Windows.Forms.TextBox();
            this.BtnCargarMaestria = new System.Windows.Forms.Button();
            this.BtnEliminarMaestria = new System.Windows.Forms.Button();
            this.BtnCargarDoctorado = new System.Windows.Forms.Button();
            this.BtnEliminarDoctorado = new System.Windows.Forms.Button();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_maestria
            // 
            resources.ApplyResources(this.txt_maestria, "txt_maestria");
            this.txt_maestria.Name = "txt_maestria";
            this.txt_maestria.TextChanged += new System.EventHandler(this.txt_maestria_TextChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txt_doctorado
            // 
            resources.ApplyResources(this.txt_doctorado, "txt_doctorado");
            this.txt_doctorado.Name = "txt_doctorado";
            this.txt_doctorado.TextChanged += new System.EventHandler(this.txt_doctorado_TextChanged);
            // 
            // BtnCargarMaestria
            // 
            this.BtnCargarMaestria.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            resources.ApplyResources(this.BtnCargarMaestria, "BtnCargarMaestria");
            this.BtnCargarMaestria.Name = "BtnCargarMaestria";
            this.BtnCargarMaestria.UseVisualStyleBackColor = false;
            this.BtnCargarMaestria.Click += new System.EventHandler(this.BtnCargarMaestria_Click);
            // 
            // BtnEliminarMaestria
            // 
            this.BtnEliminarMaestria.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            resources.ApplyResources(this.BtnEliminarMaestria, "BtnEliminarMaestria");
            this.BtnEliminarMaestria.Name = "BtnEliminarMaestria";
            this.BtnEliminarMaestria.UseVisualStyleBackColor = false;
            this.BtnEliminarMaestria.Click += new System.EventHandler(this.BtnEliminarMaestria_Click);
            // 
            // BtnCargarDoctorado
            // 
            this.BtnCargarDoctorado.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            resources.ApplyResources(this.BtnCargarDoctorado, "BtnCargarDoctorado");
            this.BtnCargarDoctorado.Name = "BtnCargarDoctorado";
            this.BtnCargarDoctorado.UseVisualStyleBackColor = false;
            this.BtnCargarDoctorado.Click += new System.EventHandler(this.BtnCargarDoctorado_Click);
            // 
            // BtnEliminarDoctorado
            // 
            this.BtnEliminarDoctorado.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            resources.ApplyResources(this.BtnEliminarDoctorado, "BtnEliminarDoctorado");
            this.BtnEliminarDoctorado.Name = "BtnEliminarDoctorado";
            this.BtnEliminarDoctorado.UseVisualStyleBackColor = false;
            this.BtnEliminarDoctorado.Click += new System.EventHandler(this.BtnEliminarDoctorado_Click);
            // 
            // BtnGuardar
            // 
            //this.BtnGuardar.BackgroundImage = global::ControlEscolar.Properties.Resources.icono_guardar1;
            resources.ApplyResources(this.BtnGuardar, "BtnGuardar");
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // Estudios
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.BtnEliminarDoctorado);
            this.Controls.Add(this.BtnCargarDoctorado);
            this.Controls.Add(this.BtnEliminarMaestria);
            this.Controls.Add(this.BtnCargarMaestria);
            this.Controls.Add(this.txt_doctorado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_maestria);
            this.Name = "Estudios";
            this.Load += new System.EventHandler(this.Estudios_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_maestria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_doctorado;
        private System.Windows.Forms.Button BtnCargarMaestria;
        private System.Windows.Forms.Button BtnEliminarMaestria;
        private System.Windows.Forms.Button BtnCargarDoctorado;
        private System.Windows.Forms.Button BtnEliminarDoctorado;
        private System.Windows.Forms.Button BtnGuardar;
    }
}