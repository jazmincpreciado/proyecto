﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;

namespace ControlEscolar
{
    public partial class FM_usuarios : Form
    {
        private UsuaridManejador _usuaridManejador;
        private Usuario _usuario;
        public FM_usuarios()//cp
        {
            
            InitializeComponent();
            _usuaridManejador = new UsuaridManejador();//CONECTAR CON LOGICA DE NEGOCIOS (HACER INSTANCIA)
            _usuario = new Usuario();
        }

        private void FM_usuarios_Load(object sender, EventArgs e)
        {
            //buscar usuario cuando carge la presentacion
            BuscarUsurarios("");
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();

        }
        private void ControlBotones(bool Nuevo ,bool guardar, bool Cancelar , bool eliminar)// -1 cp
        {
            btn_nuevo.Enabled = Nuevo;
            btn_eliminar.Enabled = eliminar;
            btn_guardar.Enabled = guardar;
            
            btn_cancelar.Enabled = Cancelar;
        }
        //metodo para llenar el datagrid .Actualizador 
        private void BuscarUsurarios(string filtro)//cp
        {
            Dgv_Usuarios.DataSource = _usuaridManejador.GetUsuarios(filtro);//pasar la lista 
        }

        private void btn_nuevo_Click(object sender, EventArgs e)//cp
        {
            ControlBotones( false, true, true , false);
            ControlCuadros(true);
            txt_nombre.Focus();
        }

        private bool validarUsuario()
        {
            var tupla = _usuaridManejador.ValidarUsuario(_usuario);// cachar la tupla
            var valido = tupla.Item1;//pasar
            var mensaje = tupla.Item2;//pasar
            
            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validacion",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            return valido;
        }
        private void btn_guardar_Click(object sender, EventArgs e)  // CP control ks deliminar escribir try y doble tabulador
        {
            CargarUsuario();//primero se carga el usuario 

            if (validarUsuario())// control ks +if 
            {
                ControlBotones(true, false, false, true);
                ControlCuadros(false);
                try
                {
                    
                    guardarUsuario();//se lo pasamos a guardar usuario 
                    limpiarCuadros();
                    BuscarUsurarios(""); // mandar llamar al data grip con los nuevos datos
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                } 
            }

           
        }

        //metodo para guardar
        private void CargarUsuario()//crear global el objeto para usarlo en otro lugar 
        {
            _usuario.Idusuario = Convert.ToInt32(lbl_id.Text);
            _usuario.Nombre = txt_nombre.Text;
            _usuario.Apellidopaterno = txt_apellidopaterno.Text;
            _usuario.Apellidomaterno = txt_apellidomaterno.Text;
            _usuario.Contrasenia = txt_contrasenia.Text;

        }
        private void guardarUsuario ()//cp
        {
            _usuaridManejador.guardar(_usuario);//sacar el usuario para mandarlo llamar en otro lugar
        }

        private void btn_cancelar_Click(object sender, EventArgs e)//CP
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }
        private void ControlCuadros (bool activar)//0cp controlar cuadros
        {
            txt_nombre.Enabled = activar;
            txt_apellidopaterno.Enabled = activar;
            txt_apellidomaterno.Enabled = activar;
            txt_contrasenia.Enabled = activar;
        }
        private void limpiarCuadros()//cp
        {
            txt_contrasenia.Text = "";
            txt_apellidopaterno.Text = "";
            txt_apellidomaterno.Text = "";
            txt_contrasenia.Text = "";
            txt_nombre.Text = "";
            lbl_id.Text = "0";

            
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)//CP
        {
            BuscarUsurarios(txt_buscar.Text ); //filtrar en la busqueda por letra 
        }

  
        private void btn_eliminar_Click(object sender, EventArgs e)//CP
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro","Eliminar Resgistro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    BuscarUsurarios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
           
            
        }
        private void EliminarUsuario()// CP necesitamos el id int para que elimine el dato
        {
            var IdUsuario = Dgv_Usuarios.CurrentRow.Cells["IdUsuario"].Value;//DEPENDE DEL DQATA
            _usuaridManejador.eliminar(Convert.ToInt32( IdUsuario));//EL METODO LO NECESITA INT
        }

        private void Dgv_Usuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)// CP doble click en el dtv
        {
            try
            {
                ModificarUsuario();
                BuscarUsurarios("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarUsuario()//1 cp funcion para modificar usuario
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            lbl_id.Text= Dgv_Usuarios.CurrentRow.Cells["IdUsuario"].Value.ToString();
           txt_nombre.Text = Dgv_Usuarios.CurrentRow.Cells["Nombre"].Value.ToString();
            txt_apellidopaterno.Text = Dgv_Usuarios.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            txt_apellidomaterno.Text = Dgv_Usuarios.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            txt_contrasenia.Text = Dgv_Usuarios.CurrentRow.Cells["contrasenia"].Value.ToString();



        }

        private void lbl_buscar_Click(object sender, EventArgs e)
        {

        }

        private void lbl_id_Click(object sender, EventArgs e)
        {

        }
    }
}
