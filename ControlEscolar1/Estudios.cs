﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.IO;
namespace ControlEscolar
{
    public partial class Estudios : Form
    {

        private OpenFileDialog _archivoPdf;
        private OpenFileDialog _archivoPdfd;
        private string _ruta;
        public static string doc;
        public static string ma;
        public static string v;

        public Estudios()
        {
            InitializeComponent();
            _archivoPdf = new OpenFileDialog();
            _archivoPdfd = new OpenFileDialog();
            string carpeta = Frm_Profesor.v;

            
          
            //_ruta = Application.StartupPath + "\\img\\";
        }

        private void Estudios_Load(object sender, EventArgs e)
        {
            
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            GuardarArchivoPdfm();
            GuardarArchivoPdfd();
            doc = txt_doctorado.Text;
            ma = txt_maestria.Text;
           


        }
        private void GuardarArchivoPdfm()
        {
            if (_archivoPdf.FileName != null)
            {
                if (_archivoPdf.FileName != "")
                {
                    var archivo = new FileInfo(_archivoPdf.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                           // codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }

                }

            }
        }
        

        private void BtnCargarMaestria_Click(object sender, EventArgs e)
        {
            CargarArchivoPdfm();

        }
        private void CargarArchivoPdfm()
        {
            _archivoPdf.Filter = "Archivo tipo(*.pdf) | *.pdf";
            _archivoPdf.Title = "cargar archivo";
            _archivoPdf.ShowDialog();

            if (_archivoPdf.FileName != "")
            {
                var archivo = new FileInfo(_archivoPdf.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                txt_maestria.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;

            }
        }

        private void BtnEliminarMaestria_Click(object sender, EventArgs e)
        {

            EliminarArchivoPdfm();
        }
        private void EliminarArchivoPdfm()
        {
            if (MessageBox.Show("estas seguro que deseas eliminar imagen", "eliminar imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (_archivoPdf.FileName != null)
                {
                    if (_archivoPdf.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoPdf.FileName);
                        if (Directory.Exists(_ruta)) // exixtente del archivo
                        {
                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length != 0)
                            {
                                //codigo para remplazar iamgen 
                                archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();

                                }

                            }
                        }
                    }
                }
            }
        }
        ///////////
        private void BtnCargarDoctorado_Click(object sender, EventArgs e)
        {
            CargarArchivoPdfd();
        }
        private void CargarArchivoPdfd()
        {
            _archivoPdfd.Filter = "Archivo tipo(*.pdf) | *.pdf";
            _archivoPdfd.Title = "cargar archivo";
            _archivoPdfd.ShowDialog();

            if (_archivoPdfd.FileName != "")
            {
                var archivo = new FileInfo(_archivoPdfd.FileName); // el show dialogo trae un archivo y le estamos pasando la informacion de ese archivo
                txt_doctorado.Text = archivo.Name; // obtener el nombre del archivo   // ruta :TxtImagenJpg.Text =_imagenJpg.FileName;

            }
        }

        private void BtnEliminarDoctorado_Click(object sender, EventArgs e)
        {

            EliminarArchivoPdfd();
        }
        private void EliminarArchivoPdfd()
        {
            if (MessageBox.Show("estas seguro que deseas eliminar imagen", "eliminar imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (_archivoPdfd.FileName != null)
                {
                    if (_archivoPdfd.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoPdfd.FileName);
                        if (Directory.Exists(_ruta)) // exixtente del archivo
                        {
                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length != 0)
                            {
                                //codigo para remplazar iamgen 
                                archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();

                                }

                            }
                        }
                    }
                }
            }
        }
        private void GuardarArchivoPdfd()
        {
            if (_archivoPdfd.FileName != null)
            {
                if (_archivoPdfd.FileName != "")
                {
                    var archivo = new FileInfo(_archivoPdfd.FileName);
                    if (Directory.Exists(_ruta)) // exixtente del archivo
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf"); //devuelve un arreglo 

                         FileInfo archivoAnterior;
                      if (obtenerArchivos.Length ==2)
                        {
                            //codigo para remplazar iamgen 
                            archivoAnterior = new FileInfo(obtenerArchivos[0]); //sollo permitiras un archivo .jpg 
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();

                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name); //copiar la imagen al directorio
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name); //copia ruta mas exacatamente con el mismo nombre
                    }


                }
            }
        }

        private void txt_doctorado_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txt_maestria_TextChanged(object sender, EventArgs e)
        {

        }
    }


}

