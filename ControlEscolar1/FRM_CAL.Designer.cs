﻿namespace ControlEscolar
{
    partial class FRM_CAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRM_CAL));
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.btn_AGREGAR = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TXT_PARCIAL3 = new System.Windows.Forms.TextBox();
            this.TXT_PARCIAL2 = new System.Windows.Forms.TextBox();
            this.TXT_PARCIAL1 = new System.Windows.Forms.TextBox();
            this.TXT_PARCIAL4 = new System.Windows.Forms.TextBox();
            this.DTG_CAL = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CMB_MATERIA = new System.Windows.Forms.ComboBox();
            this.CMB_ALUMNO = new System.Windows.Forms.ComboBox();
            this.CMB_GRUPO = new System.Windows.Forms.ComboBox();
            this.lbl_id = new System.Windows.Forms.Label();
            this.btn_exportar = new System.Windows.Forms.Button();
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bgvgExportar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.DTG_CAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_cancelar.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancelar.Image")));
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(852, 180);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(100, 38);
            this.btn_cancelar.TabIndex = 37;
            this.btn_cancelar.Text = "CANCELAR";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = false;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_eliminar.Image = ((System.Drawing.Image)(resources.GetObject("btn_eliminar.Image")));
            this.btn_eliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_eliminar.Location = new System.Drawing.Point(852, 136);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(100, 38);
            this.btn_eliminar.TabIndex = 36;
            this.btn_eliminar.Text = "ELIMINAR";
            this.btn_eliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_eliminar.UseVisualStyleBackColor = false;
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_guardar.Image = ((System.Drawing.Image)(resources.GetObject("btn_guardar.Image")));
            this.btn_guardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_guardar.Location = new System.Drawing.Point(852, 93);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(100, 38);
            this.btn_guardar.TabIndex = 35;
            this.btn_guardar.Text = "GUARDAR";
            this.btn_guardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_guardar.UseVisualStyleBackColor = false;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // btn_AGREGAR
            // 
            this.btn_AGREGAR.BackColor = System.Drawing.Color.White;
            this.btn_AGREGAR.Image = ((System.Drawing.Image)(resources.GetObject("btn_AGREGAR.Image")));
            this.btn_AGREGAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_AGREGAR.Location = new System.Drawing.Point(852, 51);
            this.btn_AGREGAR.Name = "btn_AGREGAR";
            this.btn_AGREGAR.Size = new System.Drawing.Size(100, 38);
            this.btn_AGREGAR.TabIndex = 34;
            this.btn_AGREGAR.Text = "AGREGAR";
            this.btn_AGREGAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_AGREGAR.UseVisualStyleBackColor = false;
            this.btn_AGREGAR.Click += new System.EventHandler(this.btn_AGREGAR_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(401, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 16);
            this.label7.TabIndex = 33;
            this.label7.Text = "PARCIAL 4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SkyBlue;
            this.label6.Location = new System.Drawing.Point(401, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 16);
            this.label6.TabIndex = 32;
            this.label6.Text = "PARCIAL 3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SkyBlue;
            this.label5.Location = new System.Drawing.Point(401, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 31;
            this.label5.Text = "PARCIAL 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(401, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 16);
            this.label4.TabIndex = 30;
            this.label4.Text = "PARCIAL 1";
            // 
            // TXT_PARCIAL3
            // 
            this.TXT_PARCIAL3.Enabled = false;
            this.TXT_PARCIAL3.Location = new System.Drawing.Point(507, 123);
            this.TXT_PARCIAL3.Name = "TXT_PARCIAL3";
            this.TXT_PARCIAL3.Size = new System.Drawing.Size(50, 20);
            this.TXT_PARCIAL3.TabIndex = 29;
            this.TXT_PARCIAL3.Text = "0";
            // 
            // TXT_PARCIAL2
            // 
            this.TXT_PARCIAL2.Enabled = false;
            this.TXT_PARCIAL2.Location = new System.Drawing.Point(507, 96);
            this.TXT_PARCIAL2.Name = "TXT_PARCIAL2";
            this.TXT_PARCIAL2.Size = new System.Drawing.Size(50, 20);
            this.TXT_PARCIAL2.TabIndex = 28;
            this.TXT_PARCIAL2.Text = "0";
            // 
            // TXT_PARCIAL1
            // 
            this.TXT_PARCIAL1.Enabled = false;
            this.TXT_PARCIAL1.Location = new System.Drawing.Point(507, 69);
            this.TXT_PARCIAL1.Name = "TXT_PARCIAL1";
            this.TXT_PARCIAL1.Size = new System.Drawing.Size(50, 20);
            this.TXT_PARCIAL1.TabIndex = 27;
            this.TXT_PARCIAL1.Text = "0";
            this.TXT_PARCIAL1.Click += new System.EventHandler(this.TXT_PARCIAL1_Click);
            // 
            // TXT_PARCIAL4
            // 
            this.TXT_PARCIAL4.Enabled = false;
            this.TXT_PARCIAL4.Location = new System.Drawing.Point(507, 149);
            this.TXT_PARCIAL4.Name = "TXT_PARCIAL4";
            this.TXT_PARCIAL4.Size = new System.Drawing.Size(50, 20);
            this.TXT_PARCIAL4.TabIndex = 26;
            this.TXT_PARCIAL4.Text = "0";
            // 
            // DTG_CAL
            // 
            this.DTG_CAL.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DTG_CAL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DTG_CAL.Location = new System.Drawing.Point(12, 240);
            this.DTG_CAL.Name = "DTG_CAL";
            this.DTG_CAL.Size = new System.Drawing.Size(940, 150);
            this.DTG_CAL.TabIndex = 25;
            this.DTG_CAL.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DTG_CAL_CellDoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(128, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 24;
            this.label3.Text = "MATERIA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(128, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 23;
            this.label2.Text = "ALUMNO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(135, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 22;
            this.label1.Text = "GRUPO";
            // 
            // CMB_MATERIA
            // 
            this.CMB_MATERIA.Enabled = false;
            this.CMB_MATERIA.Location = new System.Drawing.Point(216, 150);
            this.CMB_MATERIA.Name = "CMB_MATERIA";
            this.CMB_MATERIA.Size = new System.Drawing.Size(121, 21);
            this.CMB_MATERIA.TabIndex = 21;
            this.CMB_MATERIA.Click += new System.EventHandler(this.CMB_MATERIA_Click);
            // 
            // CMB_ALUMNO
            // 
            this.CMB_ALUMNO.Enabled = false;
            this.CMB_ALUMNO.Location = new System.Drawing.Point(216, 108);
            this.CMB_ALUMNO.Name = "CMB_ALUMNO";
            this.CMB_ALUMNO.Size = new System.Drawing.Size(121, 21);
            this.CMB_ALUMNO.TabIndex = 20;
            this.CMB_ALUMNO.SelectedIndexChanged += new System.EventHandler(this.CMB_ALUMNO_SelectedIndexChanged);
            this.CMB_ALUMNO.Click += new System.EventHandler(this.CMB_ALUMNO_Click);
            this.CMB_ALUMNO.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.CMB_ALUMNO_ChangeUICues);
            // 
            // CMB_GRUPO
            // 
            this.CMB_GRUPO.Enabled = false;
            this.CMB_GRUPO.Location = new System.Drawing.Point(216, 72);
            this.CMB_GRUPO.Name = "CMB_GRUPO";
            this.CMB_GRUPO.Size = new System.Drawing.Size(121, 21);
            this.CMB_GRUPO.TabIndex = 19;
            this.CMB_GRUPO.SelectedIndexChanged += new System.EventHandler(this.CMB_GRUPO_SelectedIndexChanged);
            this.CMB_GRUPO.Click += new System.EventHandler(this.CMB_GRUPO_Click);
            this.CMB_GRUPO.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.CMB_GRUPO_ChangeUICues);
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Location = new System.Drawing.Point(926, 22);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(13, 13);
            this.lbl_id.TabIndex = 38;
            this.lbl_id.Text = "0";
            // 
            // btn_exportar
            // 
            this.btn_exportar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_exportar.BackgroundImage")));
            this.btn_exportar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_exportar.Location = new System.Drawing.Point(917, 15);
            this.btn_exportar.Name = "btn_exportar";
            this.btn_exportar.Size = new System.Drawing.Size(35, 30);
            this.btn_exportar.TabIndex = 39;
            this.btn_exportar.UseVisualStyleBackColor = true;
            this.btn_exportar.Click += new System.EventHandler(this.btn_exportar_Click);
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(131, 12);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(380, 20);
            this.txt_buscar.TabIndex = 40;
            this.txt_buscar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(517, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // bgvgExportar
            // 
            this.bgvgExportar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgvgExportar_DoWork);
            this.bgvgExportar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgvgExportar_RunWorkerCompleted);
            // 
            // FRM_CAL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1001, 402);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.btn_exportar);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.btn_AGREGAR);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TXT_PARCIAL3);
            this.Controls.Add(this.TXT_PARCIAL2);
            this.Controls.Add(this.TXT_PARCIAL1);
            this.Controls.Add(this.TXT_PARCIAL4);
            this.Controls.Add(this.DTG_CAL);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CMB_MATERIA);
            this.Controls.Add(this.CMB_ALUMNO);
            this.Controls.Add(this.CMB_GRUPO);
            this.Name = "FRM_CAL";
            this.Text = "FRM_CAL";
            this.Load += new System.EventHandler(this.FRM_CAL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DTG_CAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Button btn_AGREGAR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TXT_PARCIAL3;
        private System.Windows.Forms.TextBox TXT_PARCIAL2;
        private System.Windows.Forms.TextBox TXT_PARCIAL1;
        private System.Windows.Forms.TextBox TXT_PARCIAL4;
        private System.Windows.Forms.DataGridView DTG_CAL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CMB_MATERIA;
        private System.Windows.Forms.ComboBox CMB_ALUMNO;
        private System.Windows.Forms.ComboBox CMB_GRUPO;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Button btn_exportar;
        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.ComponentModel.BackgroundWorker bgvgExportar;
    }
}