﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class frmPantalla_principal : Form
    {
        public frmPantalla_principal()
        {
            InitializeComponent();
        }

        private void Pantalla_principal_Load(object sender, EventArgs e)
        {

        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FM_usuarios usuarios = new FM_usuarios();
            usuarios.ShowDialog();

        }

        private void alumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Alumnos alumnos = new Frm_Alumnos();
            alumnos.ShowDialog();

        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Profesor profesor = new Frm_Profesor();
            profesor.ShowDialog();
        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materia materia = new Materia();
            materia.ShowDialog();
        }

        private void escuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_EscuelaE escuelaE = new frm_EscuelaE();
            escuelaE.ShowDialog();
        }

        private void catalogosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grupoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Grupo frm_Grupo = new Frm_Grupo();
            frm_Grupo.ShowDialog();
        }

        private void calificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_CAL fRM_CAL = new FRM_CAL();
            fRM_CAL.ShowDialog();
            
        }
    }
    }

