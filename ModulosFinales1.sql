/*MODULOS FINALES
1.Crear un catalogo que permita asignar alumnos a un grupo .Para esto se debe crear un fromulario 
que permita escribir el nombre del grupo y asignar los alumnos a este grupo .

2.Crear una asignacion de profesores con materias y con grupos en la cual se especifique que profesor impartira una materia y a que grupo.

3.Crear un modulo que permita hacer la captura de calificacion para 4 parciales de cada materia por grupo.Una vez terminada la captura se pueda 
imprimir un reporte en pdf y excell como acta de calificaciones .*/
use controlescolar;




/*ALUMNOS */
create table alumnos ( N_Control int primary key auto_increment,nombre varchar(50),
apellidopaterno varchar(50),
apellidomaterno varchar(50),
fecha_nac varchar(50),
domicilio varchar(50),
email varchar(50),
sexo varchar(1),
estados varchar(50),
ciudades varchar(50));
select *from alumnos;

create view  Vgrupo as
select grupo.id as 'ID', grupo.nombre as 'Grupo', fk_alumno as 'Matricula',alumnos.nombre as 'Nombre',apellidopaterno,apellidomaterno from grupo,alumnos where alumnos.N_Control=grupo.fk_alumno;

/*MATERIA */
create table materia (no int primary key auto_increment,
nombreM varchar(35),
idMA varchar(15),
horastMA int,
horasprMA int,
fk_materiaS varchar(35),
fk_materiaN varchar(35),

foreign key (fk_materiaS) REFERENCES materia(nombreM),
foreign key (fk_materiaN) REFERENCES materia(nombreM));
create view MateriaRel as
select no  as 'id',nombreM  as 'Materia1',idMA as 'Codigo',horastMA AS 'Teoria', horasprMA AS 'Practica',(horastMA+horasprMA) as 'Creditos', 
fk_materiaS as 'Materia 2',fk_materiaN as 'Materia 3' FROM materia 
;
/*PROFESOR */
CREATE TABLE Profesor(No_control varchar(16),Nombre varchar(25) ,Apellido_M varchar(15), Apellido_P varchar (15),Direccion varchar(50) , 
Estado varchar (20),Ciudad varchar(15),N_cedula int,Titulo varchar(29),Fecha_nac varchar(50),
foreign KEY(Estado) REFERENCES Estados(codigo),
foreign key(Ciudad) REFERENCES Ciudades(c_Ciudades));  
/*ESTUIODS*/
create table  estudios2 ( id int auto_increment primary key,
fk_nombre varchar(100),
titulo  varchar (50),
maestria varchar(50),
doctorado varchar(50),
foreign key(fk_nombre) references Profesor(No_control));
/*GRUPO*/
create table grupo( id int primary key auto_increment,
nombre varchar(25) ,
fk_alumno int,
foreign key(fk_alumno) references alumnos(N_Control));
 
 select *from grupo;

/*ASIGNACION */

CREATE TABLE Asignacion (id int primary key AUTO_INCREMENT ,
fk_profesor varchar(16),
fk_grupo INT,
fk_materia varchar(16),
foreign key (fk_profesor) references Profesor(No_Control),
foreign key(fk_grupo) references Grupo(id),
foreign key (fk_materia) references materia(idMA));

insert into Asignacion values (null,'D201901',1,'SIM08');
insert into Asignacion values (null,'D201901',1,'fg09');

SELECT *FROM Asignacion;
select *from profesor;
SELECT *FROM materia;
SELECT *FROM grupo;


create view VAsignacion as
select  Asignacion.id as 'ID',Asignacion.fk_profesor as 'Matricula' ,Profesor.Nombre as'Profesor',Asignacion.fk_grupo,Grupo.nombre as'Grupo', materia.nombreM as 'Materia' FROM Profesor,Grupo,materia, Asignacion where Asignacion.fk_profesor=Profesor.No_control 
AND Asignacion.fk_grupo=grupo.id and Asignacion.fk_materia=materia.idMA ;
 
 select *from VAsignacion;
 CREATE TABLE Calificaciones (id int primary key auto_increment ,Grupo varchar(30),Materia varchar(30),Alumno int ,parcial1 double ,parcial2 double,parcial3 double ,parcial4 double);
 
 
 
 
create view vCalificaciones AS
SELECT  calificaciones.id as 'ID',Grupo as'GRUPO',Materia as'MATERIA',Alumno as'ID ',alumnos.nombre as'ALUMNO',parcial1  AS'PARCIAL 1',parcial2 AS'PARCIAL 2',
 parcial3 AS'PARCIAL 3',parcial4 as 'PARCIAL 4' FROM CALIFICACIONES,Alumnos where alumnos.N_Control=Calificaciones.Alumno;







