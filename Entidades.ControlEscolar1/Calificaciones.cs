﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class Calificaciones
    {
        private int _id;
        private string _Grupo;
        private string _Materia;
        private int _Alumno;
        private double _parcial1;
        private double _parcial2;
        private double _parcial3;
        private double _parcial4;

        public int Id { get => _id; set => _id = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
        public int Alumno { get => _Alumno; set => _Alumno = value; }
        public double Parcial1 { get => _parcial1; set => _parcial1 = value; }
        public double Parcial2 { get => _parcial2; set => _parcial2 = value; }
        public double Parcial3 { get => _parcial3; set => _parcial3 = value; }
        public double Parcial4 { get => _parcial4; set => _parcial4 = value; }
    }
}
