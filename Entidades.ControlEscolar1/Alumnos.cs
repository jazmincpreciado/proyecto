﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Alumnos
    {
            private int _N_Control;
            private string _nombre;
            private string _apellidopaterno;
            private string _apellidomaterno;
            private string _fecha_nac;
            private string _domicilio;
            private string _email;
            private string _sexo;
            private string _estados;
            private string _ciudades;

        public int N_Control { get => _N_Control; set => _N_Control = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
        public string Fecha_nac { get => _fecha_nac; set => _fecha_nac = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
        public string Email { get => _email; set => _email = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Estados { get => _estados; set => _estados = value; }
        public string Ciudades { get => _ciudades; set => _ciudades = value; }
    }
}
