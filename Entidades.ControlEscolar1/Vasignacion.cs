﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
   public  class Vasignacion
    {
        private int _ID;
        private string _Matricula;
        private string _Profesor;
        private string _Grupo;
        private string _Materia;

        public int ID { get => _ID; set => _ID = value; }
        public string Matricula { get => _Matricula; set => _Matricula = value; }
        public string Profesor { get => _Profesor; set => _Profesor = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
    }
}
