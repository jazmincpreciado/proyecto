﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Profesor
    {
        private string _no_control;
        private string _nombre;
        private string _apellido_M;
        private string _apellido_P;
        private string _direccion;
        private string _estado;
        private string _ciudad;
        private int _n_cedula;
        private string _titulo;
        private string _fecha_nac;

        public string No_control { get => _no_control; set => _no_control = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellido_M { get => _apellido_M; set => _apellido_M = value; }
        public string Apellido_P { get => _apellido_P; set => _apellido_P = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public int N_cedula { get => _n_cedula; set => _n_cedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Fecha_nac { get => _fecha_nac; set => _fecha_nac = value; }
    }
}
