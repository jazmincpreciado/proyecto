﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Estados  //Crear una clase para obtner los atributos de la tabla  Ciudades
    {
        private string codigo;
        private string nombre;

        public string Codigo{ get => codigo; set => codigo = value; }
        public string Nombre{ get => nombre; set => nombre = value; }
    }
}
