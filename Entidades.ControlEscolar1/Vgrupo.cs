﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
   public class Vgrupo
    {
        private  int  _ID;
        private string _Grupo;
        private int _Matricula;
        private string Nombre;
        private string _apellidopaterno;
        private string _apellidomaterno;

        public int ID { get => _ID; set => _ID = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public int Matricula { get => _Matricula; set => _Matricula = value; }
        public string Nombre1 { get => Nombre; set => Nombre = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
    }
}
