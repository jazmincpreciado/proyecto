﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
   public  class VCalificaiones
    {
        private int _ID;
        private string _GRUPO;
        private string _MATERIA;
        private int _IDALUMNO;
        private string _ALUMNO;
        private double _PARCIAL1;
        private double _PARCIAL2;
        private double _PARCIAL3;
        private double _PARCIAL4;

        public int ID { get => _ID; set => _ID = value; }
        public string GRUPO { get => _GRUPO; set => _GRUPO = value; }
        public string MATERIA { get => _MATERIA; set => _MATERIA = value; }
        public int IDALUMNO { get => _IDALUMNO; set => _IDALUMNO = value; }
        public string ALUMNO { get => _ALUMNO; set => _ALUMNO = value; }
        public double PARCIAL1 { get => _PARCIAL1; set => _PARCIAL1 = value; }
        public double PARCIAL2 { get => _PARCIAL2; set => _PARCIAL2 = value; }
        public double PARCIAL3 { get => _PARCIAL3; set => _PARCIAL3 = value; }
        public double PARCIAL4 { get => _PARCIAL4; set => _PARCIAL4 = value; }
    }
}
