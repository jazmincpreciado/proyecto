﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class Grupo
    {
        private int _id;
        private string _nombre;
        private int _fk_alumno;

        public int Id { get => _id; set => _id = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public int Fk_alumno { get => _fk_alumno; set => _fk_alumno = value; }
    }
}
