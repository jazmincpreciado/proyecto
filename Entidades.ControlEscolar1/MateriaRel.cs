﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
     public class MateriaRel
    {
        private int id;
        private string _materia1;
        private string _codigo;
        private int _Teoria;
        private int _Practica;
        private int _Creditos;
        private string _Materia2;
        private string _Materia3;

        public int Id { get => id; set => id = value; }
        public string Materia1 { get => _materia1; set => _materia1 = value; }
        public string Codigo { get => _codigo; set => _codigo = value; }
        public int Teoria { get => _Teoria; set => _Teoria = value; }
        public int Practica { get => _Practica; set => _Practica = value; }
        public int Creditos { get => _Creditos; set => _Creditos = value; }
        public string Materia2 { get => _Materia2; set => _Materia2 = value; }
        public string Materia3 { get => _Materia3; set => _Materia3 = value; }
    }
}
