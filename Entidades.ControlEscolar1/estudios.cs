﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
   public  class estudios
    {
        private int _id;
        
        private string _fk_nombre;
     
        private string _titulo;
        private string _maestria;
        private string _doctorado;

        public int Id { get => _id; set => _id = value; }
        public string Fk_nombre { get => _fk_nombre; set => _fk_nombre = value; }
       
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Maestria { get => _maestria; set => _maestria = value; }
        public string Doctorado { get => _doctorado; set => _doctorado = value; }
    }
}
