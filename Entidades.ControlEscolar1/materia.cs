﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class materia
    {
        private int _no;
        private string _nombreM;
        private string _idMA;
        private int _horastMA;
        private int _horasprMA;
        private string _fk_materiaS;
        private string _fk_materiaN;

        public int No { get => _no; set => _no = value; }
        public string NombreM { get => _nombreM; set => _nombreM = value; }
        public string IdMA { get => _idMA; set => _idMA = value; }
        public int HorastMA { get => _horastMA; set => _horastMA = value; }
        public int HorasprMA { get => _horasprMA; set => _horasprMA = value; }
        public string Fk_materiaS { get => _fk_materiaS; set => _fk_materiaS = value; }
        public string Fk_materiaN { get => _fk_materiaN; set => _fk_materiaN = value; }
    }

}