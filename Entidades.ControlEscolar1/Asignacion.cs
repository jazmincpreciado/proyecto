﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Asignacion
    {
        private int _id;
        private string _fk_profesor;
        private int  _fk_grupo;
        private string _fk_materia;

        public int Id { get => _id; set => _id = value; }
        public string Fk_profesor { get => _fk_profesor; set => _fk_profesor = value; }
        
        public string Fk_materia { get => _fk_materia; set => _fk_materia = value; }
        public int Fk_grupo { get => _fk_grupo; set => _fk_grupo = value; }
    }
}
