﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class EscuelaE
    {
        private int _no;
        private string _nombre;
        private string _RFC;
        private string _domicilio;
        private int _telefono;
        private string _correoe;
        private string _pagina_w;
        private string _director;
        private string _logo;

        public int No { get => _no; set => _no = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string RFC { get => _RFC; set => _RFC = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
        public int Telefono { get => _telefono; set => _telefono = value; }
        public string Correoe { get => _correoe; set => _correoe = value; }
        public string Pagina_w { get => _pagina_w; set => _pagina_w = value; }
        public string Director { get => _director; set => _director = value; }
        public string Logo { get => _logo; set => _logo = value; }
    }
}
