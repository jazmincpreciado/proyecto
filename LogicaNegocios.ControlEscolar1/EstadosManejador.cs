﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocios.ControlEscolar
{
    public class EstadosManejador //  CREAR UNA CLASE DE CIUDADES PARA VINCULARSE CON EL ACCESO A DATOS DE CIUDADES.  
    {
        private EstadosAcceso _estadosAcceso = new EstadosAcceso();

        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = _estadosAcceso.GetEstados(filtro);

            return listEstados;
        }
    }
}

