﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;//libreria para nombre validar una cadena

namespace LogicaNegocios.ControlEscolar
{
    // Todas las validaciones si no cumple los requisitos se regresa
    public class UsuaridManejador
    {
        private UsuarioAccesoDatos _usuarioAccesoDatos = new UsuarioAccesoDatos();// INSTANCIA DE ACCESO A DATOS
        private  bool nombreValido (string nombre)//solo va vivir en la clase
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                                          //ARROBA DELIMITADOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(nombre); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool apellidopaternoValido (string apellidopaterno)
        {
            var regex= new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(apellidopaterno);
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool apellidomaternoValido(string apellidopaterno)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(apellidopaterno);
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool contraseniaValido(string contrasenia)
        {
            var regex = new Regex(@"^[0-9]+([a-zA-Z]+)*$");
            var match = regex.Match(contrasenia);
            if (match.Success)
            {
                return true;
            }
            return false;
        }




        public void guardar(Usuario usuario)//MANDAMOS EL METODO GUARADAR
        {
            _usuarioAccesoDatos.guardar(usuario);//PUENTE

        }
        public void eliminar(int idusuario)
        {
            _usuarioAccesoDatos.eliminar(idusuario);

        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            var Listusuario = _usuarioAccesoDatos.GetUsuarios(filtro);
            return Listusuario;
        }





        //metodo para retoner una tupla
        //retorna el numero de variables de diferentes tipos

        public Tuple<Boolean,string>ValidarUsuario(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;


            // Nombre 
            if (usuario.Nombre.Length==0)//vacio
            {
                mensaje =mensaje+ "El nombre de usuario es necesario \n";
                valido = false;
            }
            else if(usuario.Nombre.Length>20)//numero de caracteres
            {
                mensaje = mensaje + "El nombre de usuario solo permite un maximo de 20 caracteres \n";
                valido = false;
            }
            else if(!nombreValido(usuario.Nombre))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario \n";
                valido = false;
            }

            //APELLIDO PATERNO

                 if (usuario.Apellidopaterno.Length == 0)//vacio
                    {
                         mensaje = mensaje + "El  Apellidopaterno es necesario \n";
                          valido = false;
                    }
            
            else if (usuario.Apellidopaterno.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "El nombre de apellido paterno solo permite un maximo de 20 caracteres \n";// se suman los mensajes 
                valido = false;
            }
            else if(!apellidopaternoValido(usuario.Apellidopaterno))// formato valido
            {
                mensaje = mensaje + "Escribe un formato valido para el apellido paterno \n";
                valido = false;
            }


           //APELLIDO MATERNO
           
                if (usuario.Apellidomaterno.Length == 0)//vacio
                {
                    mensaje = mensaje + "El   Apellido materno es necesario \n";
                    valido = false;
                }
            else if (usuario.Apellidomaterno.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "El nombre de apellido materno solo permite un maximo de 20 caracteres \n";// se suman los mensajes 
                valido = false;
            }
            else if (!apellidomaternoValido(usuario.Apellidomaterno))// formato valido
            {
                mensaje = mensaje + "Escribe un formato valido para el apellido materno \n";
                valido = false;
            }

            //CONTRASEÑA
            if (usuario.Contrasenia.Length == 0)//vacio
            {
                mensaje = mensaje + "La contraseña es necesario \n";
                valido = false;
            }
            else if (usuario.Contrasenia.Length > 10)//numero de caracteres
            {
                mensaje = mensaje + "La contraseña solo permite un maximo de 10 caracteres \n";// se suman los mensajes 
                valido = false;
            }
            else if (!contraseniaValido(usuario.Contrasenia))// formato valido
            {
                mensaje = mensaje + "Escribe un formato valido para la contraseña \n Deebe contener entre 8-10 caracteres, por lo menos un digito y un alfanumérico, y no puede contener caracteres espaciales   ";
                valido = false;
            }




            return Tuple.Create(valido, mensaje);
        }

    }
}
