﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocios.ControlEscolar
{
   public class CiudadesManejador// CREAR UNA CLASE DE CIUDADES PARA VINCULARSE CON EL ACCESO A DATOS DE CIUDADES.  

    {

        private CiudadesAcceso _ciudadesAccesoDatos = new CiudadesAcceso();

        public List<Ciudaes> GetCiudades(string filtro)
        {
            var listCiudades = _ciudadesAccesoDatos.GetCiudades(filtro);

            return listCiudades;
        }
    }
}
