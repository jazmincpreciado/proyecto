﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;


namespace LogicaNegocios.ControlEscolar
{
   public  class VAsignacionManejador
    {
        private VAsignacionAccesoDatos _VAsignacionAccesoDatos = new VAsignacionAccesoDatos();

        public List<Vasignacion> GetVasignacion(string filtro)
        {
            var listvasignacion = _VAsignacionAccesoDatos.GetVAsignacion(filtro);

            return listvasignacion;
        }
    }
}
