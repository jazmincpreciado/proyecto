﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocios.ControlEscolar
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _ProfesorAccesoDatos = new ProfesorAccesoDatos();

        public void guardar(Profesor profesor)//MANDAMOS EL METODO GUARADAR
        {
            _ProfesorAccesoDatos.guardar(profesor);//PUENTE
            
        }
        public void eliminar(string idusuario)
        {
          _ProfesorAccesoDatos.eliminar(idusuario); 

        }
        
        public List<Profesor> GetProfesor(string filtro)
        {
            var Listprofesor = _ProfesorAccesoDatos.GetProfesor(filtro);
            return Listprofesor;
        }
    }
}
