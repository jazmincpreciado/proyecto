﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;


namespace LogicaNegocios.ControlEscolar
{
     public class MateriaRelManejador
    {

        private MateriaRelAccesoDatos _materiaRelAccesoDatos = new MateriaRelAccesoDatos();

        public List<MateriaRel> GetMateriaRel(string filtro)
        {
            var listv = _materiaRelAccesoDatos.GetMateriaRel(filtro);

            return listv;
        }
        public void eliminar(int no)
        {
            _materiaRelAccesoDatos.eliminar(no);

        }
    }
}
