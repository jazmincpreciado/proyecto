﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;
using System.Text.RegularExpressions;//libreria para nombre validar una cadena

namespace LogicaNegocios.ControlEscolar
{
    public class AlumnosManejador
    {
        private bool nombreValido(string nombre)//solo va vivir en la clase
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                                                //ARROBA DELIMITADOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(nombre); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool apellidopValido(string apellidop)//solo va vivir en la clase
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                                                //ARROBA DELIMITADOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(apellidop); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool apellidomValido(string apellidom)//solo va vivir en la clase
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                                                //ARROBA DELIMITADOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(apellidom); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool fechanacValido(string fecha_nac)//solo va vivir en la clase
        {
            var regex = new Regex(@"^\d{1,2}\/\d{1,2}\/\d{2,4}$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                                                //ARROBA DELIMITADOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(fecha_nac); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool emailValido(string email)//solo va vivir en la clase
        {
            var regex = new Regex(@"^[\w._%-]+@[\w.-]+\.[a-zA-Z]{2,4}$"); //la clase de la libreria ESPACIO EN EL SEGUNDO PARENTESIS (@"^[A-Za-z]+( [A-Za-z]+)*$");
                                    //ARROBA DELIMITA/[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/DOR ^INICIO CON UNA LETRA ENTRE A A LA Z MAYUSUCULA Y CONCATENAR UN ESPACIO SEGUNDO NOMBRE Y TODO ASI HASTA EL FINAL DE LA CADENA 
            var match = regex.Match(email); //comprueba que la cadena cumple co la expresion de arriba
            if (match.Success)
            {
                return true;
            }
            return false;
        }






        private AlumnoAcceso _alumnoAcceso = new AlumnoAcceso();// INSTANCIA DE ACCESO A DATOS
        public void guardar(Alumnos alumnos)//MANDAMOS EL METODO GUARADAR
        {
            _alumnoAcceso.guardar(alumnos);//PUENTE

        }
        public void eliminar(int n_control)
        {
            _alumnoAcceso.eliminar(n_control);

        }
        public List<Alumnos> GetAlumnos(string filtro)
        {
            var Listalumnos = _alumnoAcceso.GetAlumnos(filtro);
            return Listalumnos;//llamar a estados y retornar dt
        }
        



        //VALIDAR DATOS
        public Tuple<Boolean, string> ValidarAlumno(Alumnos alumnos)
        {
            string mensaje = "";
            bool valido = true;

            // Nombre 
            if (alumnos.Nombre.Length == 0)//vacio
            {
                mensaje = mensaje + "El nombre de usuario es necesario \n";
                valido = false;
            }
            else if (alumnos.Nombre.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "El nombre de usuario solo permite un maximo de 20 caracteres \n";
                valido = false;
            }
            else if (!nombreValido(alumnos.Nombre))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario \n";
                valido = false;
            }
            //APELLIDO PATERNO
            if (alumnos.Apellidopaterno.Length == 0)//vacio
            {
                mensaje = mensaje + "El Apellido Paterno del alumno  es necesario \n";
                valido = false;
            }
            else if (alumnos.Apellidopaterno.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "El Apellido Paterno solo permite un maximo de 20 caracteres \n";
                valido = false;
            }
            else if (!apellidopValido(alumnos.Apellidopaterno))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para el Apellido Paterno del alumno \n";
            }


            //APELLIDO MATERNO
            if (alumnos.Apellidomaterno.Length == 0)//vacio
            {
                mensaje = mensaje + "El Apellido Materno del alumno  es necesario \n";
                valido = false;
            }
            else if (alumnos.Apellidomaterno.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "El Apellido Materno solo permite un maximo de 20 caracteres \n";
                valido = false;
            }
            else if (!apellidomValido(alumnos.Apellidomaterno))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para el Apellido Materno del alumno \n";
            }

            //FECHA NACIMIENTO
            if (alumnos.Fecha_nac.Length == 0)//vacio
            {
                mensaje = mensaje + "La fecha de nacimiento del alumno  es necesario \n";
                valido = false;
            }
            else if (alumnos.Fecha_nac.Length > 20)//numero de caracteres
            {
                mensaje = mensaje + "La fecha de nacimiento  solo permite un maximo de 20 caracteres \n";
                valido = false;
            }
            else if (!fechanacValido(alumnos.Fecha_nac))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para  La fecha de nacimiento  \n (Por ejemplo 01 / 01 / 2007) \n";
            }

            //EMAIL 
            if (alumnos.Email.Length == 0)//vacio
            {
                mensaje = mensaje + "EL Email  es necesario \n";
                valido = false;
            }
            else if (alumnos.Email.Length > 45)//numero de caracteres
            {
                mensaje = mensaje + "El Email  solo permite un maximo de 45 caracteres \n";
                valido = false;
            }
            else if (!emailValido(alumnos.Email))// si es diferente de true entra porque me devuelve un boleano
            {
                mensaje = mensaje + "Escribe un formato valido para El Email  \n";
            }

            //DOMICILIO
            if (alumnos.Domicilio.Length == 0)//vacio
            {
                mensaje = mensaje + "EL Domicilio  es necesario \n";
                valido = false;
            }
            else if (alumnos.Domicilio.Length >45)//numero de caracteres
            {
                mensaje = mensaje + "El Domicilio  solo permite un maximo de 45 caracteres  \n";
                valido = false;
            }

            //SEXO
            if (alumnos.Sexo.Length == 0)//vacio
            {
                mensaje = mensaje + "EL Sexo  es necesario \n";
                valido = false;
            }
            else if (alumnos.Email.Length > 45)//numero de caracteres
            {
                mensaje = mensaje + "El Sexo solo permite  maximo 1 caracter M O F \n";
                valido = false;
            }




            return Tuple.Create(valido, mensaje);
        }

        public DataSet getidalumno(string filtro)
        {
            var ds = _alumnoAcceso.getidAlumno(filtro);
            return ds;
        }

    }
}
