﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;




namespace LogicaNegocios.ControlEscolar
{
     public class CalificacionesManejador
    {

        private CalificacionesAccesoDatos _CalificacionesAccesoDatos = new CalificacionesAccesoDatos();// INSTANCIA DE ACCESO A DATOS

        public void guardar(Calificaciones calificaciones)//MANDAMOS EL METODO GUARADAR
        {
            _CalificacionesAccesoDatos.guardar(calificaciones);//PUENTE

        }
        public void eliminar(int id)
        {
            _CalificacionesAccesoDatos.eliminar(id);

        }
        public List<Calificaciones> Getcalificaciones(string filtro)
        {
            var Listcal = _CalificacionesAccesoDatos.Getcalificaiones(filtro);
            return Listcal;//llamar a estados y retornar dt
        }

    }
}
