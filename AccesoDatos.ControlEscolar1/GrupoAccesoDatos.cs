﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;


namespace AccesoDatos.ControlEscolar
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;//CREACION DE LA INSTANCIA DE LA CONEXION PARA TENER ACCESO A DATOS
        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD
        }
        public void guardar(Grupo grupo)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (grupo.Id == 0)
            {
                string consulta = string.Format("insert into grupo values(null,'{0}','{1}')", grupo.Nombre, grupo.Fk_alumno);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
           
            else //
            {

                string consulta = string.Format("Update grupo set  fk_alumno ={0}, nombre='{1}' where id={2} ", grupo.Fk_alumno, grupo.Nombre,grupo.Id);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id)
        {
            string consulta = string.Format("Delete from grupo where id={0}",id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupo> GetGrupo(string filtro)
        {
            var ListGrupo = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "select *from grupo where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupo");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var grupo = new Grupo
                {
                    Id= Convert.ToInt32(row["id"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Fk_alumno =Convert.ToInt32( row["fk_alumno"].ToString())
          
                };
                ListGrupo.Add(grupo);
            }
            return ListGrupo;

        }
        public List<Grupo> GetGruponombre(string filtro)
        {
            var ListGrupon = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "select distinct nombre from grupo ";
            ds = conexion.ObtenerDatos(consulta, "grupo");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var grupo = new Grupo
                {
                   
                    Nombre = row["nombre"].ToString(),
                    

                };
                ListGrupon.Add(grupo);
            }
            return ListGrupon;

        }

        public DataSet getidgrupo(string filtro)
        {
            var ds = new DataSet();
            string consulta = "select  id from grupo where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupo");

            return ds;
        }
    }



}

