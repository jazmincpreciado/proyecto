﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;







namespace AccesoDatos.ControlEscolar
{
    public class VCalificacionesAccesoDatos
    {

        ConexionAccesoDatos conexion;

        public VCalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306);
        }
        public List<VCalificaiones> Getvcalificaicion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvCalificaciones = new List<VCalificaiones>();
            var ds = new DataSet();
            string consulta = "Select * from vCalificaciones where Grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "vCalificaciones");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var vCalificaciones = new VCalificaiones
                {

                    ID = Convert.ToInt32(row["ID"].ToString()),
                    GRUPO = row["GRUPO"].ToString(),
                    MATERIA = row["MATERIA"].ToString(),
                    IDALUMNO = Convert.ToInt32(row["ID ALUMNO"].ToString()),
                    ALUMNO = row["ALUMNO"].ToString(),
                    PARCIAL1= Convert.ToDouble(row["PARCIAL 1"].ToString()),
                    PARCIAL2 = Convert.ToDouble(row["PARCIAL 2"].ToString()),
                    PARCIAL3= Convert.ToDouble(row["PARCIAL 3"].ToString()),
                    PARCIAL4 = Convert.ToDouble(row["PARCIAL 4"].ToString()),
                };
                listvCalificaciones.Add(vCalificaciones);
            }


            return listvCalificaciones;
        }
    }

}

