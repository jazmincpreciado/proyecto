﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;


namespace AccesoDatos.ControlEscolar
{

    public class ProfesorAccesoDatos

    {
        ConexionAccesoDatos conexion;//CREACION DE LA INSTANCIA DE LA CONEXION PARA TENER ACCESO A DATOS
        public ProfesorAccesoDatos() //NAME CLASE
        {

            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD

        }
        

        private DataTable ObtenerId()
        {
            
            DateTime anio = DateTime.Now;
            //consultaC:\Windows.old\Users\jesy\Documents\5 SEMESTRE\TALLER DE BD\1 PARCIAL\ControlEscolar\AccesoDatos.ControlEscolar\ProfesorAccesoDatos.cs
           String CONSULTA=string.Format($"select max(Substring(No_Control,-2)) as 'NextNo_Control' from profesor where No_Control  like '%{anio.Year.ToString()}%'; ");
             var ds=conexion.ObtenerDatos(CONSULTA,"profesor");
            
            var dt = new DataTable();
            dt = ds.Tables[0];

            // id = string.Format($"select max(Substring(No_Control,-2)) as 'NextNo_Control' from profesor where No_Control  like '%{anio.Year.ToString()}%';");
            //Ultimo registro almacenado
            //string consulta = "select *from usuario where nombre like'%" + filtro + "%'";
            //select  max(substring(No_control,-2)) from profesor where No_Control like "%2019%";
            return dt;
        }

        public void guardar(Profesor profesor)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {

           
                DataTable id = ObtenerId();

                DateTime anio = DateTime.Now;
                string nuevaid = "";
            
            
            if (id.Rows.Count == 0)//!
                {
                    int idn = 1;
                    nuevaid = "D" + anio.Year.ToString() + 0 + idn;
                }
                else
                {
                    string c = id.Rows[0]["NextNo_Control"].ToString();

                    int idn = int.Parse(c) + 01;

                    nuevaid = "D" + anio.Year.ToString() + 0 + idn;
                }
            if(profesor.No_control == "")
            {
                string consulta = string.Format("insert into Profesor values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", nuevaid, profesor.Nombre, profesor.Apellido_M, profesor.Apellido_P, profesor.Direccion, profesor.Estado, profesor.Ciudad, profesor.N_cedula, profesor.Titulo, profesor.Fecha_nac);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
                //Console.WriteLine("entro nuevo");
            }
            else
            {
                string consulta = string.Format("Update Profesor set Nombre ='{0}' , Apellido_M='{1}' , Apellido_P ='{2}',Direccion='{3}',Estado ='{4}',Ciudad='{5}',N_cedula='{6}',Titulo='{7}', Fecha_nac ='{8}' where No_control='{9}' ", profesor.Nombre, profesor.Apellido_M, profesor.Apellido_P, profesor.Direccion, profesor.Estado, profesor.Ciudad, profesor.N_cedula, profesor.Titulo, profesor.Fecha_nac, profesor.No_control);
                conexion.EjecutarConsulta(consulta);
                //Console.WriteLine(profesor.No_control);

            }
        }
       
          
           
        



        public void eliminar(string n_control)
        {
            string consulta = string.Format("Delete from Profesor where No_control='{0}'", n_control);//////!!!!!
            conexion.EjecutarConsulta(consulta);
        }


        public List<Profesor> GetProfesor(string filtro)
        {
            var Listprofesor = new List<Profesor>();
            var ds = new DataSet();
            string consulta = "select *from Profesor where Nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta,"Profesor");
            var dt = new DataTable();
            //

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var profesor= new Profesor

                {
                    No_control = row["No_control"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apellido_M= row["Apellido_M"].ToString(),
                    Apellido_P = row["Apellido_P"].ToString(),
                    Direccion = row["Direccion"].ToString(),
                   Estado = row["Estado"].ToString(),
                    Ciudad = row["Ciudad"].ToString(),
                   N_cedula = Convert.ToInt32(row["N_cedula"]),
                    Titulo = row["Titulo"].ToString(),
                   Fecha_nac = row["Fecha_nac"].ToString()
                   
                   
                };
                Listprofesor.Add(profesor);
            }
            return Listprofesor;
        }

           
    }
}
