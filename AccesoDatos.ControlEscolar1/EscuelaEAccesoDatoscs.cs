﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;




namespace AccesoDatos.ControlEscolar
{
    public class EscuelaEAccesoDatoscs
    {
        ConexionAccesoDatos conexion;
        public EscuelaEAccesoDatoscs() //NAME CLASE
        {

            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD

        }
        public void eliminar(int no)
        {
            string consulta = string.Format("Delete from escuela where no={0}", no);
            conexion.EjecutarConsulta(consulta);

        }
        public void guardar(EscuelaE escuela)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (escuela.No == 0)
            {
                string consulta = string.Format("insert into escuela values(1,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", escuela.Nombre,escuela.RFC, escuela.Domicilio, escuela.Telefono,escuela.Correoe,escuela.Pagina_w,escuela.Director,escuela.Logo);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("Update escuela set nombre ='{0}' , RFC ='{1}',domicilio='{2}' ,telefono='{3}',correoe='{4}',pagina_w='{5}',director='{6}',logo='{7}' where no={8} ", escuela.Nombre,escuela.RFC,escuela.Domicilio, escuela.Telefono, escuela.Correoe, escuela.Pagina_w, escuela.Director, escuela.Logo,escuela.No);
                conexion.EjecutarConsulta(consulta);
                Console.WriteLine("entro");
            }


        }
        public List<EscuelaE> Getescuela(string filtro)
        {
            var Liste = new List<EscuelaE>();
            var ds = new DataSet();
            string consulta = "select *from escuela where nombre like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            //

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var escuela = new EscuelaE
                {
                    No = Convert.ToInt32(row["no"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    RFC=row["RFC"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Telefono = Convert.ToInt32(row["telefono"].ToString()),
                    Correoe = row["correoe"].ToString(),
                    Pagina_w = row["pagina_w"].ToString(),
                    Director = row["director"].ToString(),
                    Logo = row["logo"].ToString()

                };
                Liste.Add(escuela);
            }
            return Liste;

        }
        public DataSet getescuelad()
        {
            var ds = new DataSet();
            string consulta = "select *from escuela ";
            ds = conexion.ObtenerDatos(consulta, "escuela");

            return ds;
        }
    }
    
}
