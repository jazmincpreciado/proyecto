﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entidades.ControlEscolar;
using System.Data;
namespace AccesoDatos.ControlEscolar
{
    public class CalificacionesAccesoDatos
    {

        ConexionAccesoDatos conexion;//CREACION DE LA INSTANCIA DE LA CONEXION PARA TENER ACCESO A DATOS
        public CalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD
        }
        public void guardar(Calificaciones calificaciones)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (calificaciones.Id == 0)
            {
                string consulta = string.Format("insert into calificaciones values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", calificaciones.Grupo,calificaciones.Materia, calificaciones.Alumno, calificaciones.Parcial1,calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("Update calificaciones set Grupo ='{0}' , Materia ='{1}' , Alumno='{2}',parcial1='{3}',parcial2='{4}',parcial3='{5}',parcial4='{6}' where id ={7} ", calificaciones.Grupo, calificaciones.Materia, calificaciones.Alumno, calificaciones.Parcial1, calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4,calificaciones.Id);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int id)
        {
            string consulta = string.Format("Delete from calificaciones where id={0}",id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Calificaciones> Getcalificaiones(string filtro)
        {
            var Listcalificaciones = new List<Calificaciones>();
            var ds = new DataSet();
            string consulta = "select *from calificaciones where grupo like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "calificaciones");
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var calificaciones = new Calificaciones
                {
                    Id = Convert.ToInt32(row["id"]),
                    Grupo = row["Grupo"].ToString(),
                    Materia = row["Materia"].ToString(),
                    Alumno = Convert.ToInt32(row["Alumno"]),
                    Parcial1 =Convert.ToDouble( row["parcial1"].ToString()),
                    Parcial2 = Convert.ToDouble(row["parcial2"].ToString()),
                    Parcial3 = Convert.ToDouble(row["parcial3"].ToString()),
                    Parcial4 = Convert.ToDouble(row["parcial4"].ToString()),


                };
                Listcalificaciones.Add(calificaciones);
            }
            return Listcalificaciones;

        }



    }
}
