﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AlumnoAcceso
    {
        ConexionAccesoDatos conexion;//CREACION DE LA INSTANCIA DE LA CONEXION PARA TENER ACCESO A DATOS
        public AlumnoAcceso()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD
        }
        public void guardar(Alumnos alumnos)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (alumnos.N_Control == 0)
            {
                string consulta = string.Format("insert into alumnos values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",alumnos.Nombre, alumnos.Apellidopaterno, alumnos.Apellidomaterno, alumnos.Fecha_nac, alumnos.Domicilio, alumnos.Email, alumnos.Sexo , alumnos.Estados, alumnos.Ciudades );//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("Update alumnos set nombre ='{0}' , apellidopaterno ='{1}' , apellidomaterno='{2}',fecha_nac='{3}',domicilio='{4}',email='{5}',sexo='{6}',estados='{7}',ciudades='{8}'  where N_control={9} ",  alumnos.Nombre, alumnos.Apellidopaterno, alumnos.Apellidomaterno, alumnos.Fecha_nac, alumnos.Domicilio, alumnos.Email, alumnos.Sexo,alumnos.Estados,alumnos.Ciudades,alumnos.N_Control);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void eliminar(int n_control)
        {
            string consulta = string.Format("Delete from alumnos where n_control={0}", n_control);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Alumnos> GetAlumnos(string filtro)
        {
            var Listalumnos = new List<Alumnos>();
            var ds = new DataSet();
            string consulta = "select *from alumnos where N_Control like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumnos");
            var dt = new DataTable();
            //

          

            
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var alumnos = new Alumnos
                {
                   N_Control = Convert.ToInt32(row["N_Control"]),
                   Nombre = row["nombre"].ToString(),
                   Apellidopaterno = row["Apellidopaterno"].ToString(),
                   Apellidomaterno = row["Apellidomaterno"].ToString(),
                   Fecha_nac = row["Fecha_nac"].ToString(),
                   Domicilio = row["Domicilio"].ToString(),
                   Email = row["Email"].ToString(),
                   Sexo = row["Sexo"].ToString(),
                   Estados = row["Estados"].ToString(),
                   Ciudades= row["Ciudades"].ToString()

                };
                Listalumnos.Add(alumnos);
            }
            return Listalumnos;

        }
        public DataSet getidAlumno(string filtro)
        {
            var da = new DataSet();
            string consulta = "select  N_Control from alumnos where nombre like'%" + filtro + "%'";
            da = conexion.ObtenerDatos(consulta, "alumnos");
            return da;
        }


    }
}
