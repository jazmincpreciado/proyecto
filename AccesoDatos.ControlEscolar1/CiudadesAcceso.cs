﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CiudadesAcceso
    {
         ConexionAccesoDatos conexion;
        public CiudadesAcceso()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306);
        }

        public List<Ciudaes> GetCiudades(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listCiudades = new List<Ciudaes>();
            var ds = new DataSet();
            string consulta = "Select * from Ciudades where fk_codigoe like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Ciudades");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Ciudaes
                {

                    C_Ciudades = Convert.ToInt32(row["C_ciudades"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Fk_codigoe= row["fk_codigoe"].ToString()

                };
                listCiudades.Add(ciudades);
            }


            return listCiudades;
        }
    }
}
