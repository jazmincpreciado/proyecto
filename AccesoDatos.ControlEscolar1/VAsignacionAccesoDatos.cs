﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class VAsignacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VAsignacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306);
        }
        public List<Vasignacion> GetVAsignacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listVasignacion = new List<Vasignacion>();
            var ds = new DataSet();
            string consulta = "Select * from VAsignacion where Grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "VAsignacion");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var VAsignacion = new Vasignacion
                {

                    ID = Convert.ToInt32(row["ID"].ToString()),
                    
                   Matricula= row["Matricula"].ToString(),
                    Profesor = row["Profesor"].ToString(),
                    Grupo= row["Grupo"].ToString(),
                    Materia = row["Materia"].ToString()


                };
                listVasignacion.Add(VAsignacion);
            }


            return listVasignacion;
        }
    }
}
