﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
     public class VGrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VGrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306);
        }
        public List<Vgrupo> GetVgrupo(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listVgrupo = new List<Vgrupo>();
            var ds = new DataSet();
            string consulta = "Select * from Vgrupo where Grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Vgrupo");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var Vgrupo = new Vgrupo
                {

                     ID= Convert.ToInt32(row["ID"].ToString()),
                    Grupo = row["Grupo"].ToString(),
                    Matricula= Convert.ToInt32(row["Matricula"].ToString()),
                    Nombre1 = row["Nombre"].ToString(),
                    Apellidopaterno = row["Apellidopaterno"].ToString(),
                    Apellidomaterno = row["Apellidomaterno"].ToString()
            

                };
                listVgrupo.Add(Vgrupo);
            }


            return listVgrupo;
        }
    }






}
   

