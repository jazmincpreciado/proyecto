﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;



namespace AccesoDatos.ControlEscolar
{
    public class AsignacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AsignacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306); // AGRAGAR LOS ATRIBUTOS PARA HACER LA CONEXION DE LA BD
        }

        public void guardara(Asignacion asignacion)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (asignacion.Id == 0)
            {
                string consulta = string.Format("insert into asignacion values(null,'{0}','{1}','{2}')", asignacion.Fk_profesor, asignacion.Fk_grupo, asignacion.Fk_materia);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }

             else //
             {

                 string consulta = string.Format("Update asignacion set  fk_profesor ='{0}',fk_grupo='{1}',fk_materia='{2}'  where id={3} ", asignacion.Fk_profesor,asignacion.Fk_grupo,asignacion.Fk_materia,asignacion.Id);
                 conexion.EjecutarConsulta(consulta);
             }

         }
        public void eliminara(int id)
        {
            string consulta = string.Format("Delete from asignacion where id={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Asignacion> Getasignacion(string filtro)
        {
            var Listasignacion = new List<Asignacion>();
            var ds = new DataSet();
            string consulta = "select *from asignacion where fk_grupo like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "asignacion");
            var dt = new DataTable();

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var asignacion = new Asignacion
                {
                    Id = Convert.ToInt32(row["id"].ToString()),
                    Fk_profesor = row["fk_profesor"].ToString(),
                    Fk_grupo = Convert.ToInt32(row["fk_grupo"].ToString()),
                    Fk_materia = row["fk_materia"].ToString()
                };
                Listasignacion.Add(asignacion);
            }
            return Listasignacion;

        }
        




    }
}
