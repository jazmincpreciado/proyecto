﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;
namespace AccesoDatos.ControlEscolar
{
    public class materiaAAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public materiaAAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "controlescolar", 3306);
        }
        public void eliminar(int no)
        {
            string consulta = string.Format("Delete from materia where no={0}", no);
            conexion.EjecutarConsulta(consulta);
        }
        public void guardar(materia materia)// CREAR UN METODO PUBLICO DE GUARDAR PARA INSERTAR LOS VALORES DE LA TABLA , AGREGAMOS DOS CAMPOS MAS PARA INGRESAR LA CIUDAD Y EL ESTADO
        {
            if (materia.No == 0)
            {
                string consulta = string.Format("insert into materia values(null,'{0}','{1}','{2}','{3}','{4}','{5}')", materia.NombreM, materia.IdMA, materia.HorastMA, materia.HorasprMA, materia.Fk_materiaS, materia.Fk_materiaN);//variables del get en may.
                conexion.EjecutarConsulta(consulta);
            }
            else //
            {

                string consulta = string.Format("Update materia set nombreM ='{0}' ,idMA='{1}' ,horastMA='{2}',horasprMA='{3}',fk_materiaS='{4}',fk_materiaN='{5}' where no={6} ", materia.NombreM, materia.IdMA, materia.HorastMA, materia.HorasprMA, materia.Fk_materiaS, materia.Fk_materiaN, materia.No);
                conexion.EjecutarConsulta(consulta);
                Console.WriteLine("entro");
            }
        }

        public List<materia> GetMateriah(string filtro)
        {
            var Listm = new List<materia>();
            var ds = new DataSet();
            string consulta = "select *from materia where nombreM like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materia");
            var dt = new DataTable();
            //

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var materia = new materia
                {
                    No = Convert.ToInt32(row["no"].ToString()),
                    NombreM = row["nombreM"].ToString(),
                    IdMA = row["idMA"].ToString(),
                    HorastMA = Convert.ToInt32(row["horastMA"].ToString()),
                    HorasprMA = Convert.ToInt32(row["horasprMA"].ToString()),
                    Fk_materiaS = row["fk_materiaS"].ToString(),
                    Fk_materiaN = row["fk_materiaN"].ToString()

                };
                Listm.Add(materia);
            }
            return Listm;


        }
        public object GetMateria(string filtro)
        {
            var Listm = new List<materia>();
            var ds = new DataSet();
            string consulta = "select *from materia where idMA like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materia");
            var dt = new DataTable();
            //

            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)  // AGREGAR LAS COLUMNAS DE ESTADOS Y CIUDADES 
            {
                var materia = new materia
                {
                    No = Convert.ToInt32(row["no"].ToString()),
                    NombreM = row["nombreM"].ToString(),
                    IdMA = row["idMA"].ToString(),
                    HorastMA = Convert.ToInt32(row["horastMA"].ToString()),
                    HorasprMA = Convert.ToInt32(row["horasprMA"].ToString()),
                    Fk_materiaS = row["fk_materiaS"].ToString(),
                    Fk_materiaN = row["fk_materiaN"].ToString()
                };
                 Listm.Add(materia);
            
            }
            return Listm;
        }

        public DataSet getiDMATERIA(string filtro)
        {
            var ds = new DataSet();
            string consulta = "select  IdMA from materia where nombreM like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materia");

            return ds;
        }
    }
}